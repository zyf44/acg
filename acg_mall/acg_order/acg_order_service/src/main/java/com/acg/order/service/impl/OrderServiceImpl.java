package com.acg.order.service.impl;

import acg.pojo.Order;
import com.acg.order.mapper.OrderMapper;
import com.acg.order.service.OrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Override
    public Integer creatOrder(List<Order> list) {
        try {
            for (Order order : list) {
                orderMapper.insert(order);
            }
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public Integer cancelOrder(Integer userid) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userid);
        try {
            orderMapper.delete(queryWrapper);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public Order order(Integer orderid) {
        return orderMapper.selectById(orderid);
    }

    @Override
    public List<Order> orderList(Integer userid) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userid);
        return orderMapper.selectList(queryWrapper);
    }

    @Override
    public Integer changeOrder(Map<String,Object> infomap) {
        Order order = order((Integer) infomap.get("orderid"));
        if(!StringUtils.isEmpty(infomap.get("orderstatus"))){
            order.setOrderstatus((Integer) infomap.get("orderstatus"));
        }
        if(!StringUtils.isEmpty(infomap.get("deliverstate"))){
            order.setDeliverstate((Integer) infomap.get("deliverstate"));
        }
        if(!StringUtils.isEmpty(infomap.get("returnstatus"))){
            order.setReturnstatus((Integer) infomap.get("returnstatus"));
        }
        if(!StringUtils.isEmpty(infomap.get("refundstatus"))){
            order.setRefundstatus((Integer) infomap.get("refundstatus"));
        }
        if(!StringUtils.isEmpty(infomap.get("reason"))){
            order.setReason((String) infomap.get("reason"));
        }
        try{
            orderMapper.updateById(order);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public Integer pay(Integer userid) {
        try {
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userid);
            Order order = new Order();
            order.setPaystatus(1);
            orderMapper.update(order, queryWrapper);
            return 1;
        }catch(Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public IPage<Order> allOrder(Integer pagenum, Integer pagesize) {
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        IPage<Order> page = new Page<>(pagenum,pagesize);
       return orderMapper.selectPage(page,queryWrapper);
    }

    @Override
//<<<<<<< HEAD
    public IPage<Map<String, Object>> allOrderByPage(Integer pageNum, Integer pageSize) {
        IPage<Map<String, Object>> page = new Page<>(pageNum, pageSize);
//        IPage<Order> page = new Page<>(pageNum, pageSize);
        return orderMapper.allOrderByPage(page);
    }

    @Override
    public List<Map<String, Object>> listLastWeek() {
        return orderMapper.listLastWeek();
    }
//=======
    public Integer deleteOrder(Integer orderid) {
        Order order = orderMapper.selectById(orderid);
        order.setDeletestate(1);
        try{
             orderMapper.updateById(order);
             return 1;
        }catch (Exception e){
            e.printStackTrace();
            return  -1;
        }
//>>>>>>> 75cb7cfa1777c29289f385becfbe685d7954bca7
    }


/*
    private QueryWrapper<Order> queryWrapper(Map<String,Object> map){
        QueryWrapper<Order> queryWrapper = null;
        if(map != null){
            queryWrapper = new QueryWrapper<>();
            if(StringUtils.isEmpty(map.get("orderid"))){
                queryWrapper.eq("order_id",map.get("orderid"));
            }
            if(StringUtils.isEmpty(map.get("orderstatus"))){
                queryWrapper.eq("order_id",map.get("orderstatus"));
            }
            if(StringUtils.isEmpty(map.get("deliverstate"))){
                queryWrapper.eq("order_id",map.get("deliverstate"));
            }
        }
    }
    */
}
