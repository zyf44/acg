package com.acg.order.controller;

import acg.pojo.Order;
import com.acg.order.service.OrderService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderShowController {

    @Autowired
    private OrderService orderService;

    //根据用户id返回用户订单列表
    @RequestMapping("/getOrderList/{userid}")
    public String getCartByID(@PathVariable Integer userid, Model model){
        List<Order> orderList = orderService.orderList(userid);
        List<Order> orderShowList = new ArrayList<>();
        for(Order order : orderList){
            if (order.getDeletestate() == 0){
                orderShowList.add(order);
            }
        }
        List<Map<String,Object>> list = new ArrayList<>();
        for(Order order:orderShowList){
            Map<String,Object> map =new HashMap<>();
            map.put("orderid",order.getOrderid());//订单id
            map.put("userid",order.getUserid());//用户id
            map.put("addid",order.getAddid());//收货地址id
            map.put("specid",order.getSpecid());//此订单的商品规格id
            map.put("deliverstate",order.getDeliverstate());//此订单的发货状态
            map.put("orderamount",order.getOrderamount());//此订单的商品数量
            map.put("ordernumber",order.getOrdernumber());//订单号
            map.put("orderprice",order.getOrderprice());//此订单的价格
            map.put("orderstatus",order.getOrderstatus());//此订单的收货状态
            map.put("paystate",order.getPaystate());//此订单的支付方式
            map.put("ordertime",order.getOrdertime());//此订单的生成时间
            map.put("reason",order.getReason());//订单的退货申请理由
            map.put("returnstatus",order.getReturnstatus());//此订单退货状态
            map.put("refundstatus",order.getRefundstatus());//此订单退款状态
            map.put("",order.getPaystatus());//此订单的支付状态
            list.add(map);
        }
        model.addAttribute("list",list);
        return "order";
    }

    //用户删除一个不显示的订单
    @RequestMapping("/deleteOrderShow/{orderid}")
    public String deleteorder(@PathVariable Integer orderid, Model model){
        orderService.deleteOrder(orderid);
        Order order = orderService.order(orderid);
        Integer id  = order.getUserid();
        return "redirect:/order/getOrderList/"+id;
    }

    //根据订单id返回一个订单详情
    @RequestMapping("/orderDetail/{orderid}")
    public String orderInfo(@PathVariable Integer orderid, Model model){
        Order order = orderService.order(orderid);
        Map<String,Object> map =new HashMap<>();
        map.put("orderid",order.getOrderid());//订单id
        map.put("userid",order.getUserid());//用户id
        map.put("addid",order.getAddid());//收货地址id
        map.put("specid",order.getSpecid());//此订单的商品规格id
        map.put("deliverstate",order.getDeliverstate());//此订单的发货状态
        map.put("orderamount",order.getOrderamount());//此订单的商品数量
        map.put("ordernumber",order.getOrdernumber());//订单号
        map.put("orderprice",order.getOrderprice());//此订单的价格
        map.put("orderstatus",order.getOrderstatus());//此订单的收货状态
        map.put("paystate",order.getPaystate());//此订单的支付方式
        map.put("ordertime",order.getOrdertime());//此订单的生成时间
        map.put("reason",order.getReason());//订单的退货申请理由
        map.put("returnstatus",order.getReturnstatus());//此订单退货状态
        map.put("refundstatus",order.getRefundstatus());//此订单退款状态
        map.put("",order.getPaystatus());//此订单的支付状态
        model.addAttribute("map",map);
        return "orderinfo";
    }
}
