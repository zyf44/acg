package com.acg.order.service;

import acg.pojo.Order;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface OrderService extends IService<Order> {
    Integer creatOrder(List<Order> orderlist);

    Integer cancelOrder(Integer userid);

    Order order(Integer orderid);

    List<Order> orderList(Integer userid);

    Integer changeOrder(Map<String, Object> infomap);

    Integer pay(Integer userid);

//<<<<<<< HEAD
    IPage<Order> allOrder(Integer pagenum,Integer pagesize);

    IPage<Map<String, Object>> allOrderByPage(Integer pageNum,Integer pageSize);

    List<Map<String, Object>> listLastWeek();
//=======
//    public IPage<Order> allOrder(Integer pagenum,Integer pagesize);

    public Integer deleteOrder(Integer orderid);
//>>>>>>> 75cb7cfa1777c29289f385becfbe685d7954bca7
}