package com.acg.order.mapper;

import acg.pojo.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;


@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    @Select("SELECT * FROM acg_order, acg_user.acg_user, acg_user.acg_address where acg_order.user_id = acg_user.user_id" +
            " and acg_user.acg_address.user_id = acg_user.user_id and acg_user.acg_address.add_id = acg_order.add_id")
    @Results(id = "orderMap", value = {
            @Result(id = true, column = "user_id", property = "userId", jdbcType = JdbcType.INTEGER),
            @Result(id = true, column = "order_id", property = "orderId", jdbcType = JdbcType.INTEGER),
            @Result(column = "specs_id", property = "specsId", jdbcType = JdbcType.INTEGER),
            @Result(column = "creation_time", property = "creationTime", jdbcType = JdbcType.VARCHAR),
            @Result(column = "pay_state", property = "payState", jdbcType = JdbcType.INTEGER),
            @Result(column = "pay_status", property = "payStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "refund_status", property = "refundStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "order_status", property = "orderStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "return_status", property = "returnStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "user_status", property = "userStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "user_name", property = "userName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_number", property = "orderNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_time", property = "orderTime", jdbcType = JdbcType.VARCHAR),
            @Result(column = "add_id", property = "addId", jdbcType = JdbcType.INTEGER),
            @Result(column = "add_phone", property = "addPhone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "add_consignee", property = "addConsignee", jdbcType = JdbcType.VARCHAR),
            @Result(column = "add_country", property = "addCountry", jdbcType = JdbcType.VARCHAR),
            @Result(column = "add_city", property = "addCity", jdbcType = JdbcType.VARCHAR),
            @Result(column = "add_province", property = "addProvince", jdbcType = JdbcType.VARCHAR),
            @Result(column = "detailed_address", property = "detailedAddress", jdbcType = JdbcType.VARCHAR),
            @Result(column = "user_sex", property = "userSex", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_amount", property = "orderAmount", jdbcType = JdbcType.INTEGER),
            @Result(column = "nick_name", property = "nickName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "deliver_state", property = "deliverState", jdbcType = JdbcType.INTEGER),
            @Result(column = "return_state", property = "returnState", jdbcType = JdbcType.INTEGER),
            @Result(column = "user_phone", property = "userPhone", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_price", property = "orderPrice", jdbcType = JdbcType.DECIMAL)
    })
    IPage<Map<String, Object>> allOrderByPage(IPage<Map<String, Object>> page);

    @Select("SELECT * FROM (select * from acg_order where DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(order_time)) r1, " +
            "acg_user.acg_user, acg_user.acg_address where r1.user_id = acg_user.user_id " +
            "and acg_user.acg_address.user_id = acg_user.user_id and acg_user.acg_address.add_id = r1.add_id")
    @ResultMap(value = "orderMap")
    List<Map<String, Object>> listLastWeek();

}
