package com.acg.order.controller;

import acg.config.entity.ResponseData;
import acg.pojo.Cart;
import acg.pojo.Order;

import com.acg.order.service.OrderService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RestController
@RequestMapping("/order")
@Api(tags = "对订单的操作")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/createOrder")
    @ApiOperation(value = "创建用户的所有订单", notes = "将新的订单信息更新到数据库")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "用户id", required = false, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "addid", value = "收货地址id", required = false, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "paystate", value = "支付方式", required = false, dataType = "Integer", paramType = "query")
    })
    public ResponseData<Order> createOrder(@RequestParam Integer userid, @RequestParam Integer addid, @RequestParam Integer paystate) {
        Cart[] carts = restTemplate.getForObject("http://CART-SERVICE/cart/getCheckedCart/" + userid, Cart[].class);
        List<Cart> cartlist = Arrays.asList(carts);
        List<Order> orderlist = new ArrayList<>();
        for (Cart cart : cartlist) {
            Order order = new Order();
            Long currentTime = System.currentTimeMillis();
            Long ordernumber = currentTime + new Random().nextInt(100);
            order.setOrdernumber(ordernumber);
            order.setAddid(addid);
            order.setOrderamount(cart.getCartamount());
            order.setSpecid(cart.getSpecsid());
            order.setOrderprice(cart.getCartprice());
            order.setDeliverstate(0);
            order.setOrderstatus(0);
            order.setOrdertime(new Date());
            order.setUserid(userid);
            order.setReturnstatus(0);
            order.setRefundstatus(0);
            order.setPaystate(paystate);
            order.setPaystatus(1);
            order.setDeletestate(0);
            orderlist.add(order);
        }
        Integer res = orderService.creatOrder(orderlist);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/deleteOrder")
    @ApiOperation(value = "撤销用户订单", notes = "根据用户id撤销用户订单")
    @ApiImplicitParam(value = "用户id", name = "userid", required = false, dataType = "Integer", paramType = "query")
    public ResponseData<Order> deleteCart(@RequestParam Integer userid) {
        Integer res = orderService.cancelOrder(userid);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/orderList")
    @ApiOperation(value = "返回用户订单列表", notes = "返回用户所有订单")
    @ApiImplicitParam(value = "用户id", name = "userid", required = false, dataType = "Integer", paramType = "query")
    public ResponseData<Order> orderList(@RequestParam Integer userid) {
        List<Order> orderlist = orderService.orderList(userid);
        if (orderlist != null) {
            return ResponseData.success().putDataVule("orderlist", orderlist);
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/listLastWeek")
    @ApiOperation(value = "一周内所有订单", notes = "一周内所有订单")
    public ResponseData<Map<String, Object>> listLastWeek() {
        List<Map<String, Object>> orderlist = orderService.listLastWeek();
        if (orderlist != null) {
            return ResponseData.success().putDataVule("orderlist", orderlist);
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/orderById")
    @ApiOperation(value = "根据订单id返回订单", notes = "返回一个订单详情")
    @ApiImplicitParam(value = "订单id", name = "orderid", required = false, dataType = "Integer", paramType = "query")
    public ResponseData<Order> findOrder(@RequestParam Integer orderid){
        Order order1 = orderService.order(orderid);
        if (order1 != null) {
            return ResponseData.success().putDataVule("order", order1);
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @PostMapping("/changeOrder")
    @ApiOperation(value = "改变订单状态",notes = "改变订单发货或者收货状态")
    @ApiImplicitParam(value = "存放订单更改信息",name="map",required = false,dataType = "Map",paramType = "body")
    public ResponseData<Order> changeOrder(@RequestBody Map<String,Object> map){
        Integer res = orderService.changeOrder(map);
        if(res == 1){
            return  ResponseData.success();
        }else{
            return  ResponseData.serverInternalError();
        }
    }

    @GetMapping("/pay")
    @ApiOperation(value = "支付成功", notes = "支付成功更新数据库支付信息")
    @ApiImplicitParam(value = "用户id", name = "userid", required = false, dataType = "Integer", paramType = "query")
    public ResponseData<Order> pay(@RequestParam Integer userid){
        Integer res = orderService.pay(userid);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "分页查询所有订单",notes = "分页查询所有订单")
    @GetMapping("/allOrder")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页数" ,name="num" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "每页条数" ,name="size" ,required = false,dataType = "Integer",paramType = "query")
    })
    public ResponseData<Order> allOrderPage(@RequestParam Integer num,@RequestParam Integer size){
        IPage<Order> orderPage =  orderService.allOrder(num,size);

        return ResponseData.success().putDataVule("totla", orderPage.getTotal())
                .putDataVule("pages", orderPage.getPages())
                .putDataVule("orders", orderPage.getRecords());
    }

    @ApiOperation(value = "分页查询所有订单",notes = "分页查询所有订单")
    @GetMapping("/allOrderByPage")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页数" ,name="num" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "每页条数" ,name="size" ,required = false,dataType = "Integer",paramType = "query")
    })
    public ResponseData<Map<String, Object>> allOrderByPage(@RequestParam Integer num,@RequestParam Integer size){
        IPage<Map<String, Object>> orderPage =  orderService.allOrderByPage(num, size);
        return ResponseData.success().putDataVule("totla", orderPage.getTotal())
                .putDataVule("pages", orderPage.getPages())
                .putDataVule("orders", orderPage.getRecords());
    }
}
