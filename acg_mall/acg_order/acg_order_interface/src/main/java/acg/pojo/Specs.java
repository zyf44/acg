package acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@TableName("acg_specs")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Specs implements Serializable {
    @TableId(value = "specs_id",type = IdType.AUTO)
    private Integer specsid;
    @TableField("mer_id")
    private Integer merid;
    @TableField("specs_type")
    private String specstype;
    @TableField("specs_stock")
    private Integer specsstock;
    @TableField("specs_price")
    private double specsprice;


}