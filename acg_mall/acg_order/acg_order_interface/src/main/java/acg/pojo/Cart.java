package acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@TableName("acg_cart")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @TableId(value = "cart_id",type = IdType.AUTO)
    private Integer cartid;
    @TableField("user_id")
    private Integer userid;
    @TableField("specs_id")
    private Integer specsid;
    @TableField("cart_amount")
    private Integer cartamount;
    @TableField("cart_price")
    private BigDecimal  cartprice;
    @TableField("cart_checked")
    private  Integer cartchecked;
}
