package acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName("acg_order")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {
    @TableId(value = "order_id",type = IdType.AUTO)
    private  Integer orderid;
    @TableField("order_number")
    private Long ordernumber;
    @TableField("user_id")
    private Integer userid;
    @TableField("add_id")
    private  Integer addid;
    @TableField("order_amount")
    private Integer orderamount;
    @TableField("specs_id")
    private Integer specid;
    @TableField("order_price")
    private BigDecimal orderprice;
    @TableField("order_time")
    private Date ordertime;
    @TableField("order_status")
    private Integer orderstatus;
    @TableField("pay_status")
    private Integer paystatus;
    @TableField("deliver_state")
    private Integer deliverstate;
    @TableField("return_status")
    private Integer returnstatus;
    @TableField("refund_status")
    private Integer refundstatus;
    @TableField("reson")
    private String reason;
    @TableField("pay_state")
    private Integer paystate;
    @TableField("delete_state")
    private Integer deletestate;
}
