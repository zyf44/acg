package com.acg.admin.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@TableName("acg_admin")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin implements Serializable {
    @TableId(value = "admin_id",type = IdType.AUTO)
    private Integer adminId;
    @TableId("admin_name")
    private  String adminName;
    @TableField("admin_password")
    private String adminPassword;
    @TableId("admin_phone")
    private String adminPhone;
    @TableField("power_status")
    private Integer powerStatus;
    @TableField("admin_power")
    private String adminPower;
//    private Integer[] adminPower;
//    private List adminPower;
    @TableField("admin_avatar")
    private String adminAvatar;

}
