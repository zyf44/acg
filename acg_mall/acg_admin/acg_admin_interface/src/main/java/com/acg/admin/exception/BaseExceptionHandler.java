package com.acg.admin.exception;



import com.acg.admin.config.entity.ResponseData;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class BaseExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseData error(Exception e){
        e.printStackTrace();
        System.out.println("调用了公共异常处理类");
        return  new ResponseData(500,e.getMessage());
    }
}
