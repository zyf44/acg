package com.acg.admin.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminReturn {


    private Integer adminId;
    private  String adminName;
    private String adminPassword;
    private String adminPhone;
    private Integer powerStatus;
//    private String adminPower;
    //    private Integer[] adminPower;
    private List adminPower;
    private String adminAvatar;
}
