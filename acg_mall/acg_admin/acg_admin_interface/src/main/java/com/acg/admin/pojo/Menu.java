package com.acg.admin.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@TableName("acg_menu")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Menu {
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;
    private String menuTitle;
    private String menuIcon;
    private String menuPath;
    private String menuComponent;
    private String menuName;
    private Integer menuLevel;
}
