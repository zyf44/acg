package com.acg.admin.mapper;

import com.acg.admin.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMApper extends BaseMapper<Admin> {
}
