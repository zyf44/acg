package com.acg.admin.mapper;

import com.acg.admin.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("select * from acg_power left join acg_menu on acg_power.menu_id = acg_menu.menu_id where admin_id = #{adminId}")
    @Results({
            @Result(id = true, column = "admin_id", property = "adminId", jdbcType = JdbcType.INTEGER),
            @Result(id = true, column = "menu_id", property = "menuId", jdbcType = JdbcType.INTEGER),
            @Result(column = "menu_title", property = "menuTitle", jdbcType = JdbcType.VARCHAR),
            @Result(column = "menu_icon", property = "menuIcon", jdbcType = JdbcType.VARCHAR),
            @Result(column = "menu_component", property = "menuComponent", jdbcType = JdbcType.VARCHAR),
            @Result(column = "menu_path", property = "menuPath", jdbcType = JdbcType.VARCHAR),
            @Result(column = "menu_level", property = "menuLevel", jdbcType = JdbcType.VARCHAR),
            @Result(column = "menu_name", property = "menuName", jdbcType = JdbcType.VARCHAR)
    })
    List<Map<String, Object>> listPowerByAdminId(Integer adminId);

}
