package com.acg.admin.service;

import com.acg.admin.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface MenuService  extends IService<Menu> {

    List<Map<String, Object>> listPowerByAdminId(Integer adminId);

}
