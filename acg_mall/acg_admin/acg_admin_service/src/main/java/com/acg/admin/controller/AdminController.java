package com.acg.admin.controller;

import com.acg.admin.config.entity.ResponseData;
import com.acg.admin.pojo.Admin;
import com.acg.admin.pojo.AdminReturn;
import com.acg.admin.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/admin")
@Api(tags = "管理员模块")
public class AdminController  {
    @Autowired
    private AdminService adminService;
    @GetMapping("/getAll")
    @ApiOperation(value = "无条件查询所有",notes = "查询所有用户的信息")
    public ResponseData<AdminReturn> getAll(){
//        List<Admin> adminList =adminService.findAll();
        List<Admin> adminList = adminService.list();
        if(adminList!= null){
            List<AdminReturn> adminReturnList = new ArrayList<>();
            for (Admin admin : adminList) {
                String[] strings = admin.getAdminPower().split(", ");
                System.out.println(strings);
                Integer[] array = (Integer[])ConvertUtils.convert(strings, Integer.class);
                System.out.println(array);
                adminReturnList.add(new AdminReturn(admin.getAdminId(), admin.getAdminName(), admin.getAdminPassword(), admin.getAdminPhone(),
                         admin.getPowerStatus(), Arrays.asList(array), admin.getAdminAvatar()));
            }
            return  ResponseData.success().putDataVule("adminList",adminReturnList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
/*    @GetMapping("/findByPhone")
    @ApiOperation(value = "查询单个管理员信息根据电话",notes = "根据电话查询单个管理员信息")
    @ApiImplicitParam(value = "用户电话号码",name = "phone",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Admin> findByPhone(String phone){
        Admin admin =adminService.GetAdminByPhone(phone);
        if(admin!= null){
            return  ResponseData.success().putDataVule("admin",admin);
        }else{
            return ResponseData.serverInternalError();
        }

    }*/
    @GetMapping("/findByAid")
    @ApiOperation(value = "查询单个管理员信息根据id",notes = "根据管理员id查询单个管理员信息")
    @ApiImplicitParam(value = "管理员id",name = "id",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Admin> findByAid(Integer adminid){
        Admin admin =adminService.findByid(adminid);
        if(admin!= null){
            return  ResponseData.success().putDataVule("admin",admin);
        }else{
            return ResponseData.serverInternalError();
        }

    }
/*    @PostMapping("/InsertAdmin")
    @ApiOperation(value = "添加管理员",notes = "添加新的管理员")
    @ApiImplicitParam(value = "添加新管理员信息",name = "admin",required = true, dataTypeClass = Admin.class,paramType = "body")
    public ResponseData<Admin> InsertAdmin(@RequestBody Admin admin){
        Integer result =adminService.insertAdmin(admin);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }*/
    @PostMapping("/InsertAdmin")
    @ApiOperation(value = "添加管理员",notes = "添加新的管理员")
    @ApiImplicitParam(value = "添加新管理员信息",name = "admin",required = true, dataTypeClass = AdminReturn.class,paramType = "body")
    public ResponseData<Admin> InsertAdmin(@RequestBody AdminReturn adminReturn){
        String adminPower = adminReturn.getAdminPower().toString();
        Admin admin = new Admin(adminReturn.getAdminId(), adminReturn.getAdminName(), adminReturn.getAdminPassword(),
                adminReturn.getAdminPhone(), adminReturn.getPowerStatus(), adminPower.substring(1, adminPower.length() - 1), adminReturn.getAdminAvatar());
        Integer result =adminService.insertAdmin(admin);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @PutMapping("/UpdateAdmin")
    @ApiOperation(value = "更新管理员",notes = "更新管理员")
    @ApiImplicitParam(value = "更新管理员信息",name = "admin",required = true, dataTypeClass = Admin.class,paramType = "body")
    public ResponseData<Admin> UpdateAdmin(@RequestBody AdminReturn adminReturn){
//        Integer result =adminService.updateAdmin(admin);
        List list = adminReturn.getAdminPower();
        String adminPower = adminReturn.getAdminPower().toString();
        Admin admin = new Admin(adminReturn.getAdminId(), adminReturn.getAdminName(), adminReturn.getAdminPassword(),
                adminReturn.getAdminPhone(), adminReturn.getPowerStatus(), adminPower.substring(1, adminPower.length() - 1), adminReturn.getAdminAvatar());
        System.out.println(admin);
        Boolean result = adminService.updateById(admin);
        if(result){
            return  ResponseData.success().putDataVule("result", result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @DeleteMapping("/DeleteAdmin")
    @ApiOperation(value = "删除管理员",notes = "根据id删除管理员")
    @ApiImplicitParam(value = "管理员id",name = "id",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Admin> UpdateAdmin(Integer id){
        Integer result =adminService.deleteAdmin(id);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }

    }
    @PostMapping("/Login")
    @ApiOperation(value = "验证登录",notes = "验证登录")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "登录账号（电话号码）" ,name="phone" ,required = true,dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "登录密码" ,name="pass" ,required = true,dataType = "String",paramType = "query")
    })
    public ResponseData<Admin> Login(String  phone, String pass){
        String password = adminService.passWordByPhone(phone);
        //解密
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        Boolean result =bcryptPasswordEncoder.matches(pass,password);
        if(result==true){
            System.out.println("登录成功！");
            return  ResponseData.success().putDataVule("result",result);
        }else{
            System.out.println("账号或密码出错");
            return ResponseData.serverInternalError();
        }
    }
    @PutMapping("/UpdatePassword")
    @ApiOperation(value = "更改密码",notes = "更改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "管理员id" ,name="id" ,required = true,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "新的密码" ,name="pass" ,required = true,dataType = "String",paramType = "query")
    })
    public ResponseData<Admin> UpdatePassword(Integer id,String pass){
        Admin admin =adminService.findByid(id);
        if(admin!=null){
            //加密
            BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
            String newpass =bcryptPasswordEncoder.encode(pass);
            admin.setAdminPassword(newpass);
            Integer result= adminService.updateAdmin(admin);
            if(result!=null){
                return  ResponseData.success().putDataVule("result",result);
            }
        }
        return ResponseData.serverInternalError();
    }

    @GetMapping("/findByPhone")
    @ApiOperation(value = "查询单个管理员信息根据电话",notes = "根据电话查询单个管理员信息")
    @ApiImplicitParam(value = "用户电话号码",name = "phone",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Admin> findByPhone(String phone){
        Admin admin =adminService.GetAdminByPhone(phone);
        String[] strings = admin.getAdminPower().split(", ");
        System.out.println(strings);
        Integer[] array = (Integer[])ConvertUtils.convert(strings, Integer.class);
        System.out.println(array);
        AdminReturn adminReturn = new AdminReturn(admin.getAdminId(), admin.getAdminName(), admin.getAdminPassword(), admin.getAdminPhone(),
                admin.getPowerStatus(), Arrays.asList(array), admin.getAdminAvatar());
        if(admin!= null){
            return  ResponseData.success().putDataVule("admin",adminReturn);
        }else{
            return ResponseData.serverInternalError();
        }

    }
}
