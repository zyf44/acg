package com.acg.admin.controller;

import com.acg.admin.config.entity.ResponseData;
import com.acg.admin.pojo.Menu;
import com.acg.admin.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RequestMapping("/menu")
@RestController
public class MenuController {
    @Autowired
    MenuService menuService;

    @GetMapping("/list")
    public ResponseData<Menu> list() {
        List<Menu> menuList = menuService.list();
        if (menuList != null) {
            return ResponseData.success().putDataVule("menuList", menuList);
        } else {
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/listByAdminId/{adminId}")
    public ResponseData<Menu> listByAdminId(@PathVariable("adminId") Integer adminId) {
        List<Map<String, Object>> menuList = menuService.listPowerByAdminId(adminId);

        if (menuList != null) {
            return ResponseData.success().putDataVule("menuList", menuList);
        } else {
            return ResponseData.serverInternalError();
        }
    }

}
