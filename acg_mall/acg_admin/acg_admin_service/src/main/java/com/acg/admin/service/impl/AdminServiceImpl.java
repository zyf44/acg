package com.acg.admin.service.impl;

import com.acg.admin.mapper.AdminMApper;
import com.acg.admin.pojo.Admin;
import com.acg.admin.service.AdminService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMApper,Admin> implements AdminService {
    @Autowired
    private AdminMApper adminMApper;
    @Override
    public List<Admin> findAll() {
        return adminMApper.selectList(null);
    }

    @Override
    public Admin findByid(Integer id) {
        return adminMApper.selectById(id);
    }

    @Override
    public Integer insertAdmin(Admin admin) {
        String password =admin.getAdminPassword();
        //使用BCryptPasswordEncoder进行加密
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPass = bcryptPasswordEncoder.encode(password);
        admin.setAdminPassword(hashPass);
        return adminMApper.insert(admin);
    }

    @Override
    public Integer updateAdmin(Admin admin) {
        return adminMApper.updateById(admin);
    }

    @Override
    public Integer deleteAdmin(Integer id) {
        return adminMApper.deleteById(id);
    }

    @Override
    public String passWordByPhone(String phone) {
        Admin admin = adminMApper.selectOne(new QueryWrapper<Admin>().eq("admin_phone",phone));
        if (admin !=null){
            return admin.getAdminPassword();
        }else {
            return  null;
        }
    }

    @Override
    public Admin GetAdminByPhone(String phone) {
        Admin admin= adminMApper.selectOne(new QueryWrapper<Admin>().eq(
                "admin_phone",phone));
        if(admin!=null){
            return  admin;
        }else{
            return null;
        }
    }

    @Override
    public Boolean CheckPhone(String phone) {
        Boolean flag =true;
        List<Admin> adminList =adminMApper.selectList(null);
        for(Admin admin : adminList){
            if(admin.getAdminPhone().equals(phone)){
                flag=false;
            }
        }
        return flag;
    }

}
