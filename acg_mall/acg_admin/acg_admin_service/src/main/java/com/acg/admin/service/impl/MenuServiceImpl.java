package com.acg.admin.service.impl;

import com.acg.admin.mapper.MenuMapper;
import com.acg.admin.pojo.Menu;
import com.acg.admin.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    MenuMapper menuMapper;

    @Override
    public List<Map<String, Object>> listPowerByAdminId(Integer adminId) {
        return menuMapper.listPowerByAdminId(adminId);
    }

}
