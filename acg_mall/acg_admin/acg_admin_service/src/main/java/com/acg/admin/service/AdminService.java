package com.acg.admin.service;

import com.acg.admin.pojo.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface AdminService extends IService<Admin> {
    List<Admin> findAll();
    Admin findByid(Integer uid);
    Integer insertAdmin(Admin user);
    Integer updateAdmin(Admin user);
    Integer deleteAdmin(Integer id);
    //根据电话（登录账号）返回密码
    String passWordByPhone(String Phone);
    //根据电话获取用户
    Admin GetAdminByPhone(String  phone);
    //检查电话号码是否唯一
    Boolean CheckPhone(String phone);
}
