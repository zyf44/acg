package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
@TableName("acg_category")
public class Category implements Serializable {
    @TableId(value = "cat_id",type = IdType.AUTO)
    private Integer catId;
    @TableField("parent_id")
    private Integer parentId;
    @TableField("cat_name")
    private String catName;
    @TableField("cat_status")
    private Integer catStatus;


    public Category() {
    }

    public Category(Integer catId, Integer parentId, String catName, Integer catStatus) {
        this.catId = catId;
        this.parentId = parentId;
        this.catName = catName;
        this.catStatus = catStatus;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getCatStatus() {
        return catStatus;
    }

    public void setCatStatus(Integer catStatus) {
        this.catStatus = catStatus;
    }

    @Override
    public String toString() {
        return "Category{" +
                "catId=" + catId +
                ", parentId=" + parentId +
                ", catName='" + catName + '\'' +
                ", catStatus=" + catStatus +
                '}';
    }
}
