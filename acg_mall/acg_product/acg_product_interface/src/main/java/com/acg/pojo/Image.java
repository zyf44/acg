package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@TableName("acg_image")
public class Image implements Serializable {
    @TableId(value = "img_id",type = IdType.AUTO)
    private Integer imgId;
    @TableField("mer_id")
    private Integer merId;
    @TableField("img_show")
    private String imgShow;
    @TableField("img_more")
    private String imgMore;
    public Image() {
    }

    public Image(Integer imgId, Integer merId, String imgShow, String imgMore) {
        this.imgId = imgId;
        this.merId = merId;
        this.imgShow = imgShow;
        this.imgMore = imgMore;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public Integer getMerId() {
        return merId;
    }

    public void setMerId(Integer merId) {
        this.merId = merId;
    }

    public String getImgShow() {
        return imgShow;
    }

    public void setImgShow(String imgShow) {
        this.imgShow = imgShow;
    }

    public String getImgMore() {
        return imgMore;
    }

    public void setImgMore(String imgMore) {
        this.imgMore = imgMore;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imgId=" + imgId +
                ", merId=" + merId +
                ", imgShow='" + imgShow + '\'' +
                ", imgMore='" + imgMore + '\'' +
                '}';
    }
}
