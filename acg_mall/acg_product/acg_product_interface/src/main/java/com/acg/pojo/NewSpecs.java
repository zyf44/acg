package com.acg.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewSpecs {

    private Integer specsId;

    private Integer merId;

//<<<<<<< HEAD
////    private SpecsDetails specsType;
//    private Map specsType;
//=======
    private Map<String,Object> specsType;
//>>>>>>> 75fbada36b87b1d9170aaf4a2b0533f6be44fb8d

    private Integer specsStock;

    private double specsPrice;
//
//<<<<<<< HEAD
//=======
//    public NewSpecs() {
//    }
//
//    public NewSpecs(Integer specsId, Integer merId, Map<String,Object> specsType, Integer specsStock, double specsPrice) {
//        this.specsId = specsId;
//        this.merId = merId;
//        this.specsType = specsType;
//        this.specsStock = specsStock;
//        this.specsPrice = specsPrice;
//    }
//>>>>>>> 75fbada36b87b1d9170aaf4a2b0533f6be44fb8d
}
