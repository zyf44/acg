package com.acg.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImageDetails implements Serializable {
    private String image;
}
