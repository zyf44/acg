package com.acg.pojo;


import lombok.Data;

import java.util.List;


public class NewImages  {
    private Integer imgId;
    private Integer merId;
    private List<ImageDetails> imgShow;
    private List<ImageDetails> imgMore;

    public NewImages() {
    }

    public NewImages(Integer imgId, Integer merId, List<ImageDetails> imgShow, List<ImageDetails> imgMore) {
        this.imgId = imgId;
        this.merId = merId;
        this.imgShow = imgShow;
        this.imgMore = imgMore;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public Integer getMerId() {
        return merId;
    }

    public void setMerId(Integer merId) {
        this.merId = merId;
    }

    public List<ImageDetails> getImgShow() {
        return imgShow;
    }

    public void setImgShow(List<ImageDetails> imgShow) {
        this.imgShow = imgShow;
    }

    public List<ImageDetails> getImgMore() {
        return imgMore;
    }

    public void setImgMore(List<ImageDetails> imgMore) {
        this.imgMore = imgMore;
    }

    @Override
    public String toString() {
        return "NewImages{" +
                "imgId=" + imgId +
                ", merId=" + merId +
                ", imgShow=" + imgShow +
                ", imgMore=" + imgMore +
                '}';
    }
}
