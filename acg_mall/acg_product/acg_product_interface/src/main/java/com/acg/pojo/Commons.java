package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("acg_comment")
@Data
public class Commons {
    @TableId(value = "com_id",type = IdType.AUTO)
    private Integer comid;
    @TableField("user_id")
    private Integer userid;
    @TableField("content")
    private String content;
    @TableField("status")
    private String status;
    @TableField("mer_id")
    private Integer merid;
    @TableField("liked_count")
    private Integer likedcount;
    @TableField("comment_time")
    private Date commenttime;

    public Commons() {
    }

    public Commons(Integer com_id, Integer user_id, String content, String status, Integer mer_id, Integer liked_count, Date comment_time) {
        this.comid = com_id;
        this.userid = user_id;
        this.content = content;
        this.status = status;
        this.merid = mer_id;
        this.likedcount = liked_count;
        this.commenttime = comment_time;
    }
}
