package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@TableName("acg_specs")
@Data
public class Specs implements Serializable {
    @TableId(value = "specs_id",type = IdType.AUTO)
    private Integer specsId;
    @TableField("mer_id")
    private Integer merId;
    @TableField("specs_type")
    private String specsType;
    @TableField("specs_stock")
    private Integer specsStock;
    @TableField("specs_price")
    private double specsPrice;

    public Specs() {
    }

    public Specs(Integer specsId, Integer merId, String specsType, Integer specsStock, double specSprice) {
        this.specsId = specsId;
        this.merId = merId;
        this.specsType = specsType;
        this.specsStock = specsStock;
        this.specsPrice = specSprice;
    }
}
