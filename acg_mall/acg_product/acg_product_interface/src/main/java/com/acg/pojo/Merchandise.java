package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;


import java.io.Serializable;
import java.util.Date;
@TableName("acg_merchandise")
@Data
public class Merchandise implements Serializable {
    @TableId(value = "mer_id" ,type = IdType.AUTO)
    private Integer merId;
    @Field("id")
    @TableField(exist = false)
    private String id;
    @TableField("sort_id")
    @Field("sort_id")
    private Integer sortId;
    @TableField("mer_name")
    @Field("mer_name")
    private String merName;
    @TableField("mer_ip")
    @Field("mer_ip")
    private String merIp;
    @TableField("mer_start_time")
    @Field("mer_start_time")
    private Date merStartTimme;
    @TableField("mer_end_time")
    @Field("mer_end_time")
    private Date merEndTime;
    @TableField("mer_introduction")
    @Field("mer_introduction")
    private  String merIntroduction;
    @TableField("mer_status")
    @Field("mer_status")
    private Integer merStatus;
    @TableField("mer_address")
    @Field("mer_address")
    private String merAddress;
    @TableField("mer_brand")
    @Field("mer_brand")
    private String merBrand;
    @TableField("mer_post")
    @Field("mer_post")
    private Integer merPost;
    @TableField("mer_post_way")
    @Field("mer_post_way")
    private String merPostWay;
    @TableField("mer_collection")
    @Field("mer_collection")
    private Integer merCollection;
    @TableField("mer_image")
    @Field("mer_image")
    private String merImage;
    @TableField("mer_buy")
    @Field("mer_buy")
    private Integer merBuy;
    @TableField("mer_response")
    @Field("mer_response")
    private Integer merresponse;
    @TableField("parent_id")
    @Field("parent_id")
    private Integer parentId;



    public Merchandise() {
    }
    public Merchandise(Integer sortId, String merName, String merIp, String merIntroduction, String merAddress, Integer merPost, String merPostWay) {
        this.sortId = sortId;
        this.merName = merName;
        this.merIp = merIp;
        this.merIntroduction = merIntroduction;
        this.merAddress = merAddress;
        this.merPost = merPost;
        this.merPostWay = merPostWay;
    }

    public Merchandise(Integer merId, Integer sortId, String merName, String merIp, Date merStartTimme, Date merEndTime, String merIntroduction, Integer merStatus, String merAddress, String merBrand, Integer merPost, String merPostWay, Integer merCollection, String merImage, Integer merBuy, Integer merresponse, Integer parentId) {
        this.merId = merId;
        this.sortId = sortId;
        this.merName = merName;
        this.merIp = merIp;
        this.merStartTimme = merStartTimme;
        this.merEndTime = merEndTime;
        this.merIntroduction = merIntroduction;
        this.merStatus = merStatus;
        this.merAddress = merAddress;
        this.merBrand = merBrand;
        this.merPost = merPost;
        this.merPostWay = merPostWay;
        this.merCollection = merCollection;
        this.merImage = merImage;
        this.merBuy = merBuy;
        this.merresponse = merresponse;
        this.parentId = parentId;
    }
}

