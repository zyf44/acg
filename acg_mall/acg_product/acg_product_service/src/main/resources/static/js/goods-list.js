var goodsType = 0;
var goodsList = [];
var pagenum = 1;
var pagesize = 6;
var total = 0;
var queryMap = {};
var isMore = 0;
$(window).ready(function () {
    goodsType = $("#goods-type").text();
    console.log(goodsType);
    queryMap = {"parentId": goodsType};
    getGoodsList();
});
$(window).scroll(function () {
    // 文档内容实际高度（包括超出视窗的溢出部分）
    var scrollHeight = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
    //滚动条滚动距离
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
    //窗口可视范围高度
    var clientHeight = window.innerHeight || Math.min(document.documentElement.clientHeight, document.body.clientHeight);
    if (clientHeight + scrollTop + 100 >= scrollHeight && isMore == 1) {
        isMore = 0;
        loadMore();
    };
});
function loadMore() {
    if(pagenum * pagesize - pagesize <= total) {
        getGoodsList();
    }else {
        $(".is-more").text("没有更多了");
    };
    console.log("num=" + pagenum + ",size=" + pagesize + ",total=" + total);
};
function selectType(sortId) {
    //全部
    if(sortId == 1){
        queryMap = {"parentId": goodsType};
    }else{
        queryMap = {"sortId": sortId};
    };
    isMore = 0;
    pagenum = 1;
    $(".goods-big-box").remove();
    $(".is-more").text("下拉加载更多");
    getGoodsList();
};
function keywords() {
    var keywords = $(".goods-search-area").val();
    queryMap = {"parentId": goodsType, "merName": keywords};
    isMore = 0;
    pagenum = 1;
    $(".goods-big-box").remove();
    $(".is-more").text("下拉加载更多");
    getGoodsList();
};
function selectSort(sortWay) {
    //销量
    if(sortWay == 1){
        queryMap = {"parentId": goodsType, "orderByDesc": "mer_buy"};
    };
    //收藏量
    if(sortWay == 2){
        queryMap = {"parentId": goodsType, "orderByDesc": "mer_collection"};
    };
    isMore = 0;
    pagenum = 1;
    $(".goods-big-box").remove();
    $(".is-more").text("下拉加载更多");
    getGoodsList();
};
function getGoodsList() {
    //请求参数
    var list = {};
    //
    $.ajax({
        //请求方式
        type : "POST",
        //请求的媒体类型
        contentType: "application/json",
        //请求地址
        url : "/merchandies/SelectPageByCondition?pagenum=" + pagenum + "&pagesize=" + pagesize,
        //数据，json字符串
        data : JSON.stringify(queryMap),
        //请求成功
        success : function(result) {
            console.log(result);
            goodsList = result.data.merListPage;
            total = result.data.totla;
            if(total <= pagesize) {
                $(".is-more").text("没有更多了");
            };
            if(total == 0) {
                $(".is-more").text("没有找到哦");
            };
            $(goodsList).each(function() {
                this.merPrice = findGoodsPrice(this.merId);
                var htmlBody = document.getElementById("goods-body");
                var goodsBox = document.createElement("div");//开始创建一个img标签
                goodsBox.setAttribute('class','goods-big-box');
                goodsBox.setAttribute('onclick','openDetail('+this.merId+')');
                var goodsPic = document.createElement("div");
                goodsPic.setAttribute('class','goods-big-box-pic');
                var goodsImg = document.createElement("img");
                goodsImg.setAttribute('class','goods-big-box-img');
                goodsImg.src = this.merImage;goodsPic.appendChild(goodsImg);
                goodsBox.appendChild(goodsPic);
                //商品名称
                var goodsName = document.createElement("div");
                goodsName.setAttribute('class','goods-big-box-name');
                goodsName.innerHTML = this.merName;
                goodsBox.appendChild(goodsName);
                //商品介绍
                var goodsIntroduce = document.createElement("div");
                goodsIntroduce.setAttribute('class','goods-big-box-introduce');
                goodsIntroduce.innerText = this.merIntroduction;
                goodsBox.appendChild(goodsIntroduce);
                //商品价格
                var goodsPrice = document.createElement("div");
                goodsPrice.setAttribute('class','goods-big-box-price');
                goodsPrice.innerText = this.merPrice + "元";
                goodsBox.appendChild(goodsPrice);
                //商品销量
                var goodsSalenum = document.createElement("div");
                goodsSalenum.setAttribute('class','goods-big-box-salenum');
                goodsSalenum.innerText = this.merBuy + "人已买";
                goodsBox.appendChild(goodsSalenum);
                //商品收藏量
                var goodsColnum = document.createElement("div");
                goodsColnum.setAttribute('class','goods-big-box-colnum');
                goodsColnum.innerText = this.merCollection + "人收藏";
                goodsBox.appendChild(goodsColnum);
                htmlBody.appendChild(goodsBox);//将这个创建好的追加到div容器里面
            });
            pagenum += 1;
            isMore = 1;
        },
        //请求失败，包含具体的错误信息
        error : function(e) {
            console.log(e.status);
            console.log(e.responseText);
        }
    });
};
var price = 0;
function findGoodsPrice(merId) {
    $.ajax({
        //请求方式
        type : "GET",
        //请求的媒体类型
        contentType: "application/json",
        //请求地址
        url : "/specs/selectListByMerid?merid="+merId,
        //数据，json字符串
        data : null,
        async:false,//将同步标志改为false，代表执行完后续代码才返回结果
        //请求成功
        success : function(result) {
            price = result.data.specsList[0].specsPrice;
        },
        //请求失败，包含具体的错误信息
        error : function(e) {
            console.log(e.status);
            console.log(e.responseText);
        }
    });
    return price;
}
function openDetail(goodsId) {
    console.log(goodsId);
    window.open('/goods/getMerchandis/' + goodsId);
}