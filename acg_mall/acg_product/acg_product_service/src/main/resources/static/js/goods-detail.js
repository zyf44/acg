function addCart() {
    var userId = $.cookie("userId");
    var specsId = $(".goods-specs").val();
    var cartAmount = $(".goods-info-num").val();
    var cartPrice = $(".goods-info-pricenum").text() * cartAmount;
    var queryMap = {"cartamount": cartAmount, "cartchecked": 0, "cartprice": cartPrice, "specsid": specsId, "userid": userId};
    console.log(queryMap);
    $.ajax({
        //请求方式
        type : "POST",
        //请求的媒体类型
        contentType: "application/json",
        //请求地址
        url : "http://121.199.17.118:8091/cart/addCart",
        //数据，json字符串
        data : JSON.stringify(queryMap),
        //请求成功
        success : function(result) {
            console.log(result);
            alert("已添加到购物车");
        },
        //请求失败，包含具体的错误信息
        error : function(e) {
            console.log(e.status);
            console.log(e.responseText);
            alert("添加失败");
        }
    });
};
function changePrice() {
    var price = $('.goods-specs option:selected').attr("price");
    $(".goods-info-pricenum").text(parseInt(price));
}