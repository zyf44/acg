package com.acg.merchandise.service;

import com.acg.pojo.Commons;
import com.baomidou.mybatisplus.extension.service.IService;



public interface CommonsService extends IService<Commons> {

    Commons findbyMerid(Integer merid);
    Integer insertCommons(Commons commons);

}
