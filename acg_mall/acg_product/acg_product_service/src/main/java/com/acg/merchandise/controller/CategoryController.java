package com.acg.merchandise.controller;

import com.acg.config.entity.ResponseData;
import com.acg.merchandise.service.CategoryService;
import com.acg.pojo.Category;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
@Api(tags = "分类列表")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @ApiOperation(value = "无条件查询所有",notes = "查询所有分类的信息")
    @GetMapping("/getCategoryAll")
    public ResponseData<Category> getCategoryAll(){
        List<Category> categoryList =  categoryService.findAll();
        if(categoryList!= null){
            return  ResponseData.success().putDataVule("categoryList",categoryList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询单条记录",notes = "根据分类id查询分类的信息")
    @GetMapping("/getCategoryByid")
    @ApiImplicitParam(value = "分类id",name = "id",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Category> getCategoryByid(Integer id){
        Category category =  categoryService.findByid(id);
        if(category!= null){
            return  ResponseData.success().putDataVule("category",category);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "根据父类id查询子类",notes = "根据父类id查询分类的信息")
    @GetMapping("/getCategoryByParentid")
    @ApiImplicitParam(value = "父类id",name = "id",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Category> getCategoryByParentid(Integer id){
        List<Category> categoryList =  categoryService.findByparentid(id);
        if(categoryList!= null){
            return  ResponseData.success().putDataVule("categoryList",categoryList);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "新增分类",notes = "新增一个分类")
    @PostMapping("/insertCategory")
    @ApiImplicitParam(value = "查询条件" ,name="category" ,required = true,dataTypeClass = Category.class,paramType = "body")
    public ResponseData<Category> insertCategory(@RequestBody Category category){
        Integer result =  categoryService.insertCategory(category);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "修改一条记录",notes = "修改一条记录")
    @PutMapping("/updateCategory")
    @ApiImplicitParam(value = "修改内容" ,name="category" ,required = true,dataType = "Category",paramType = "body")
    public ResponseData<Category> updateCategory(@RequestBody Category category){
        Integer result =  categoryService.updateCategory(category);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "删除一条记录",notes = "根据id删除分类")
    @DeleteMapping("/deleteCategory")
    @ApiImplicitParam(value = "分类id" ,name="id" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Category> deleteCategory( Integer id){
        Integer result =  categoryService.deleteCategory(id);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

}
