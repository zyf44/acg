package com.acg.merchandise.service;

import com.acg.pojo.Merchandise;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface MerchandiseService extends IService<Merchandise> {
     List<Merchandise> findAll();
     Merchandise findByid(Integer id);
     Integer insertMerchandise(Merchandise merchandise);
     Integer deleteMerchandise(Integer id);
     Integer updateMerchandise(Merchandise merchandise);
     Integer putoffMerchandise(Integer id);
    List<Merchandise> QueryByCondition( Map<String,Object> queryMap);
     IPage<Merchandise> MerListPage(Integer pagenum, Integer pagesize);
     IPage<Merchandise> QueryConditionPage(Map<String, Object> queryMap, Integer pagenum, Integer pagesize);

     List<Merchandise> listLastWeek();

}
