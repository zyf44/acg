package com.acg.merchandise.controller;

import com.acg.config.entity.ResponseData;
import com.acg.merchandise.service.CommonsService;
import com.acg.pojo.Commons;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commons")
@Api(tags = "评论接口")
public class CommonsController {
    @Autowired
    private CommonsService commonsService;

    @ApiOperation(value = "评论" , notes = "评论")
    @PostMapping("/insertCommons")
    @ApiImplicitParam(value = "评论" ,name = "commons" , required = true , dataTypeClass = Commons.class , paramType = "body")
    public ResponseData<Commons> insertCommons(@RequestBody Commons commons){
        Integer result = commonsService.insertCommons(commons);
        if (result != null){
            return ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "查询评论", notes = "根据id查询")
    @GetMapping("/getCommonsByid")
    @ApiImplicitParam(value = "查询id" , name = "id", required = true , dataType = "Integer" , paramType = "Query")
    public ResponseData<Commons> getCommonsByid(Integer id){
        Commons commons = commonsService.findbyMerid(id);
        if (commons != null){
            return ResponseData.success().putDataVule("commons",commons);
        }else{
            return ResponseData.serverInternalError();
        }
    }
}
