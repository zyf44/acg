package com.acg.merchandise.service.impl;

import com.acg.pojo.Merchandise;
import com.acg.merchandise.mapper.MerchandiseMapper;
import com.acg.merchandise.service.MerchandiseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class MerchandiseServiceImpl extends ServiceImpl<MerchandiseMapper, Merchandise> implements MerchandiseService {
    @Autowired
    private MerchandiseMapper merchandiseMapper;
    @Autowired
    SolrClient solrClient;
    @Override
    //查询列表中的所有商品
    public List<Merchandise> findAll() {
        List<Merchandise> list = merchandiseMapper.selectList(null);
        return list;
    }

    @Override
    public Merchandise findByid(Integer id) {
        Merchandise bean = null;
        try {
            System.out.println("id: "+ id);
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", "id:" + id);
            QueryResponse query = solrClient.query(solrQuery);
            List<Merchandise> beans = query.getBeans(Merchandise.class);
            System.out.println(beans);
            bean = beans.get(0);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bean;
    }
    //查询条件(可输入的条件有sortid mername merstarttime merip meraddress merbrand mmerpost 以及升降序orderByAsc/orderByDesc)
    private QueryWrapper<Merchandise> queryWrapper1(Map<String,Object> queryMap){
        QueryWrapper<Merchandise> queryWrapper = null;
        if(queryMap !=null){
            queryWrapper = new QueryWrapper<>();
            if(!StringUtils.isEmpty(queryMap.get("sortId"))){
                queryWrapper.eq("sort_id",queryMap.get("sortId"));
            }
            if(!StringUtils.isEmpty(queryMap.get("parentId"))){
                queryWrapper.eq("parent_id",queryMap.get("parentId"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merName"))){
                queryWrapper.like("mer_name",queryMap.get("merName"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merStartTime"))){
                //大于等于创建时间 create_time》=所给时间
                queryWrapper.ge("mer_start_time",queryMap.get("merStartTime"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merIp"))){
                queryWrapper.like("mer_ip", queryMap.get("merIp"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merAddress"))){
                queryWrapper.like("mer_address", queryMap.get("merAddress"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merBrand"))){
                queryWrapper.like("mer_brand", queryMap.get("merBrand"));
            }
            if(!StringUtils.isEmpty(queryMap.get("merPost"))){
                queryWrapper.eq("mer_post", queryMap.get("merPost"));
            }
            if(!StringUtils.isEmpty(queryMap.get("orderByAsc"))){
                queryWrapper.orderByAsc((String) queryMap.get("orderByAsc"));
            }
            if(!StringUtils.isEmpty(queryMap.get("orderByDesc"))){
                queryWrapper.orderByDesc((String) queryMap.get("orderByDesc"));
            }
        }
        return  queryWrapper;
    }
    private SolrQuery solrQueryWrapper(Map<String,Object> queryMap){
        SolrQuery query = new SolrQuery();
        Boolean hasQ = false;
        query.set("q", "*:*");
        if (!StringUtils.isEmpty(queryMap.get("merName"))) {
            query.set("q", "mer_name:" + queryMap.get("merName") + " OR " + "mer_introduction:" + queryMap.get("merName"));
            hasQ = true;
//            query.addFilterQuery("mer_name:" + queryMap.get("merName"));
//            query.addFilterQuery("mer_introduction:" + queryMap.get("merIntroduction"));
            query.setHighlight(true);
            query.addHighlightField("mer_name");
            query.setHighlightSimplePre("<font color='red'>");
            query.setHighlightSimplePost("</font>");
        }
        if (!StringUtils.isEmpty(queryMap.get("merIp"))) {
            if (hasQ) {
                query.addFilterQuery("mer_ip:" + queryMap.get("merIp"));
            } else {
                query.set("q", "mer_ip:" + queryMap.get("merIp"));
                hasQ = true;
            }
        }
        if (!StringUtils.isEmpty(queryMap.get("merBrand"))) {
            if (hasQ) {
                query.addFilterQuery("mer_brand:" + queryMap.get("merBrand"));
            } else {
                query.set("q", "mer_brand:" + queryMap.get("merBrand"));
                hasQ = true;
            }
        }
        if (!StringUtils.isEmpty(queryMap.get("sortId"))) {
            if (hasQ) {
                query.addFilterQuery("sort_id:" + queryMap.get("sortId"));
            } else {
                hasQ = true;
                query.set("q", "sort_id:" + queryMap.get("sortId"));
            }
        }
        if (!StringUtils.isEmpty(queryMap.get("orderByDesc"))) {
            query.addSort((String) queryMap.get("orderByDesc"), SolrQuery.ORDER.desc);
        }
        if (!StringUtils.isEmpty(queryMap.get("orderByAsc"))) {
            query.addSort((String) queryMap.get("orderByAsc"), SolrQuery.ORDER.asc);
        }
        return  query;
    }
    @Override
    //新增一个商品
    public Integer insertMerchandise(Merchandise merchandise) {
        Date creattime =new Date();
        merchandise.setMerStartTimme(creattime);
        merchandise.setMerStatus(1);
        try {
            merchandise.setId(String.valueOf(merchandise.getMerId()));
            solrClient.addBean(merchandise);
            solrClient.commit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
        return merchandiseMapper.insert(merchandise);
    }

    @Override
    //删除商品
    public Integer deleteMerchandise(Integer id) {
        if (findByid(id) != null){
            try {
                solrClient.deleteById(String.valueOf(id));
            } catch (SolrServerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return merchandiseMapper.deleteById(id);
        }
       else{
           return  null;
        }
    }

    @Override
    //更新商品信息
    public Integer updateMerchandise(Merchandise merchandise) {
        try {
            if (findByid(merchandise.getMerId()) != null){
                merchandise.setId(String.valueOf(merchandise.getMerId()));
                solrClient.addBean(merchandise);
                solrClient.commit();
                return merchandiseMapper.updateById(merchandise);
            }
            else{
                return  null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    //下架商品
    public Integer putoffMerchandise(Integer id) {
        Merchandise merchandise =findByid(id);
        if(merchandise!=null){
            Date endtime =new Date();
            merchandise.setMerEndTime(endtime);
            merchandise.setMerStatus(0);
            return merchandiseMapper.updateById(merchandise);
        }
        else {
            return null;
        }
    }

    @Override
    public List<Merchandise> QueryByCondition(Map<String, Object> queryMap) {
        return merchandiseMapper.selectList(queryWrapper1(queryMap));
    }



    @Override
    public IPage<Merchandise> MerListPage(Integer pagenum, Integer pagesize) {
        IPage<Merchandise> page = new Page<>(pagenum, pagesize);
//        page = merchandiseMapper.selectPage(page,null);
        try {
            System.out.println("solrClient: " + solrClient);
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", "*:*");
            solrQuery.setStart((pagenum - 1) * pagesize);
            solrQuery.setRows(pagesize);
            QueryResponse queryResponse = solrClient.query(solrQuery);
            List<Merchandise> beans = queryResponse.getBeans(Merchandise.class);
            for (Merchandise bean : beans) {
                bean.setMerId(Integer.parseInt(bean.getId()));
                System.out.println(bean);
            }
            long count = queryResponse.getResults().getNumFound();
            page.setTotal(count);
            page.setRecords(beans);
            page.setPages(count / pagesize + 1);
            page.setCurrent(queryResponse.getResults().getStart() + 1);
            page.setSize(pagesize);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return page;
    }

    @Override
    public IPage<Merchandise> QueryConditionPage(Map<String, Object> queryMap, Integer pagenum, Integer pagesize) {
        IPage<Merchandise> page = new Page<>();
        for (Map.Entry<String, Object> entry : queryMap.entrySet()) {
            System.out.println(entry.getKey() + ":\t" + entry.getValue());
        }
        SolrQuery query = solrQueryWrapper(queryMap);
        QueryResponse queryResponse = null;
        try {
            query.setStart((pagenum - 1) * pagesize);
            query.setRows(pagesize);
            queryResponse = solrClient.query(query);
            long count = queryResponse.getResults().getNumFound();
            System.out.println(count);
            List<Merchandise> beans = queryResponse.getBeans(Merchandise.class);
            Map<String, Map<String, List<String>>> highlighting = queryResponse.getHighlighting();
            for (Merchandise bean : beans) {
                bean.setMerId(Integer.valueOf(bean.getId()));
                if (highlighting != null) {
                    Map<String, List<String>> stringListMap = highlighting.get(bean.getId());
                    if (stringListMap.get("mer_name") != null) {
                        bean.setMerName(stringListMap.get("mer_name").get(0));
                    }
                }
            }
            page.setTotal(count);
            page.setRecords(beans);
            page.setPages(count / pagesize + 1);
            page.setCurrent(queryResponse.getResults().getStart() + 1);
            page.setSize(pagesize);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return page;
    }

    @Override
    public List<Merchandise> listLastWeek() {
        List<Merchandise> beans = null;
        try {
            SolrQuery solrQuery = new SolrQuery();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Calendar calendarWeek = Calendar.getInstance();
            calendarWeek.add(Calendar.DAY_OF_MONTH, -7);
            Date weekDate = calendarWeek.getTime();
            Date nowDate = new Date();
            System.out.println(sdf.format(weekDate));
            System.out.println(sdf.format(nowDate));
            System.out.println("mer_start_time:[" + sdf.format(weekDate) + " TO " + sdf.format(nowDate) + "]");
            solrQuery.set("q", "mer_start_time:[" + sdf.format(weekDate) + " TO " + sdf.format(nowDate) + "]");
            QueryResponse query = solrClient.query(solrQuery);
            beans = query.getBeans(Merchandise.class);
            for (Merchandise bean : beans) {
                bean.setMerId(Integer.valueOf(bean.getId()));
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return beans;
    }
}