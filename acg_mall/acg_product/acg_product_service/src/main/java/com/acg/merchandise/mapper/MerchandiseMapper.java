package com.acg.merchandise.mapper;

import com.acg.pojo.Merchandise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface MerchandiseMapper extends BaseMapper<Merchandise> {

    @Select("select * from acg_merchandise where DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(mer_start_time)")
    List<Merchandise> listLastWeek();
}
