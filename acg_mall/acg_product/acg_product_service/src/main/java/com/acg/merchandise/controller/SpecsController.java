package com.acg.merchandise.controller;

import com.acg.config.entity.ResponseData;
import com.acg.merchandise.service.SpecsService;
import com.acg.pojo.NewSpecs;
import com.acg.pojo.Specs;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/specs")
@Api(tags = "规格列表")
public class SpecsController {
    @Autowired
    private SpecsService specsService;

    @ApiOperation(value = "无条件查询所有",notes = "查询所有规格的信息")
    @GetMapping("/getSpecsAll")
    public ResponseData<Specs> getSpecsAll(){
        List<Specs> specsList =  specsService.findAll();
    if(specsList!= null){
        List<NewSpecs>  newSpecsList =new ArrayList<>();
        for (int i=0;i<specsList.size();i++){
           Map<String,Object> specsType =JSONObject.parseObject(specsList.get(i).getSpecsType());
            System.out.println(specsType);
            NewSpecs newSpecs =new NewSpecs(specsList.get(i).getSpecsId(),specsList.get(i).getMerId(),specsType,specsList.get(i).getSpecsStock(),specsList.get(i).getSpecsPrice());
            newSpecsList.add(newSpecs);
        }
            return  ResponseData.success().putDataVule("specsList",newSpecsList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询规格",notes = "根据merid查询规格")
    @GetMapping("/selectListByMerid")
    @ApiImplicitParam(value = "查询规格" ,name="merid" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Specs> selectListByMerid(Integer merid){
        List<Specs>  specsList=  specsService.findbyMerid(merid);
        if(specsList!= null){
            List<NewSpecs>  newSpecsList =new ArrayList<>();
            for (int i=0;i<specsList.size();i++){
                Map<String,Object> specsType =JSONObject.parseObject(specsList.get(i).getSpecsType());
                System.out.println(specsType);
                NewSpecs newSpecs =new NewSpecs(specsList.get(i).getSpecsId(),specsList.get(i).getMerId(),specsType,specsList.get(i).getSpecsStock(),specsList.get(i).getSpecsPrice());
                newSpecsList.add(newSpecs);
            }
            return  ResponseData.success().putDataVule("specsList",newSpecsList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询商品和规格",notes = "根据specsId查询规格")
    @GetMapping("/oneBySpecsId/{specsId}")
    @ApiImplicitParam(value = "查询商品和规格" ,name="specsId" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Map<String, Object>> oneBySpecsId(@PathVariable("specsId") Integer specsId){
        Map<String, Object>  merDetail=  specsService.oneBySpecsId(specsId);
        if(merDetail!= null){
            return  ResponseData.success().putDataVule("merDetail",merDetail);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "新增规格",notes = "新增一个规格")
    @PostMapping("/insertSpecs")
    @ApiImplicitParam(value = "新增规格" ,name="newSpecs" ,required = true,dataTypeClass = NewSpecs.class,paramType = "body")
    public ResponseData<Specs> insertSpecs(@RequestBody NewSpecs newSpecs){
        Integer result =  specsService.insertSpecs(newSpecs);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "修改规格",notes = "修改一个规格")
    @PutMapping("/updateSpecs")
    @ApiImplicitParam(value = "修改规格" ,name="newSpecs" ,required = true,dataTypeClass = NewSpecs.class,paramType = "body")
    public ResponseData<Specs> updateSpecs(@RequestBody NewSpecs newSpecs){
//    public ResponseData<Specs> updateSpecs(@RequestBody Specs specs){
        Integer result =  specsService.updateSpece(newSpecs);
//        boolean result = specsService.updateById(specs);
        if(result != -1){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "删除规格",notes = "根据id删除一个规格")
    @DeleteMapping("/deleteSpecs")
    @ApiImplicitParam(value = "删除规格" ,name="id" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Specs> deleteSpecs(Integer id){
        Integer result =  specsService.deleteSpecs(id);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

}
