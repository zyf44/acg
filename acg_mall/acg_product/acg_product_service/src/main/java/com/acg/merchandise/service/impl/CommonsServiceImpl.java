package com.acg.merchandise.service.impl;

import com.acg.merchandise.mapper.CommonsMapper;
import com.acg.merchandise.service.CommonsService;
import com.acg.pojo.Commons;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonsServiceImpl extends ServiceImpl<CommonsMapper,Commons> implements CommonsService{

    @Autowired
    private CommonsMapper commonsMapper;

    @Override
    public Commons findbyMerid(Integer merid) {
        return commonsMapper.selectById(merid);
    }

    @Override
    public Integer insertCommons(Commons commons) {
        return commonsMapper.insert(commons);
    }
}
