package com.acg.merchandise.mapper;

import com.acg.pojo.Specs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

@Mapper
public interface SpecsMapper extends BaseMapper<Specs> {

    @Select("select * from acg_specs, acg_merchandise where acg_specs.mer_id = acg_merchandise.mer_id and specs_id = #{specsId}")
    @Results({
            @Result(id = true, column = "specs_id", property = "specsId", jdbcType = JdbcType.INTEGER),
            @Result(id = true, column = "mer_id", property = "merId", jdbcType = JdbcType.INTEGER),
            @Result(column = "specs_type", property = "specsType", jdbcType = JdbcType.VARCHAR),
            @Result(column = "specs_stock", property = "specsStock", jdbcType = JdbcType.INTEGER),
            @Result(column = "mer_stock", property = "merStock", jdbcType = JdbcType.INTEGER),
            @Result(column = "mer_ip", property = "merIp", jdbcType = JdbcType.VARCHAR),
            @Result(column = "mer_name", property = "merName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "mer_image", property = "merImage", jdbcType = JdbcType.VARCHAR)
    })
    Map<String, Object> oneBySpecsId(Integer specsId);

}
