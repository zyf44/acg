package com.acg.merchandise.mapper;

import com.acg.pojo.Commons;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommonsMapper extends BaseMapper<Commons> {
}
