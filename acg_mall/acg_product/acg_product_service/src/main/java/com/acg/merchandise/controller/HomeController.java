package com.acg.merchandise.controller;


import com.acg.merchandise.service.CategoryService;
import com.acg.merchandise.service.MerchandiseService;
import com.acg.merchandise.service.SpecsService;
import com.acg.pojo.Merchandise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/home")
public class HomeController {
    @Autowired
    private MerchandiseService merchandiseService;
    @Autowired
    private SpecsService specsService;
    @Autowired
    private CategoryService categoryService;

    //获取人气手办和周边
    @RequestMapping("/getpopular")
    public String getpopularGarage(Model model){
        List<Map<String,Object>> popularGarage= getPoplar(1004);
        List<Map<String,Object>> popularPeriphery=getPoplar(1002);
        System.out.println(popularGarage+"----------"+popularPeriphery);
        model.addAttribute("popularGarage",popularGarage);
        model.addAttribute("popularPeriphery",popularPeriphery);
        return "home";
    }
    private   List<Map<String,Object>> getPoplar(int parentid ){
        Map<String,Object> map=null;
        Map<String,Object> map1 =new HashMap<>();
        map1.put("orderByDesc","mer_buy");
        List<Merchandise> merchandises =merchandiseService.QueryByCondition(map1);
        int size=merchandises.size();
        List<Merchandise> newmerchandises =new ArrayList<>();
        for(int i=0;i<size;i++){
            if(categoryService.findByid(merchandises.get(i).getSortId()).getParentId() == parentid){
                System.out.println(i+"是所需的");
                newmerchandises.add(merchandises.get(i));
            }else {
                System.out.println(categoryService.findByid(merchandises.get(i).getSortId()).getParentId() +"是不需要的");
            }
        }
        System.out.println(newmerchandises);
        List<Map<String,Object>> list =new ArrayList<>();
        for(int j =0;j<newmerchandises.size();j++){
            map=new HashMap<>();
            map.put("merid",newmerchandises.get(j).getMerId());
            map.put("name",newmerchandises.get(j).getMerName());
            map.put("image",newmerchandises.get(j).getMerImage());
            map.put("price",(int)specsService.LowestPriceByMerid(newmerchandises.get(j).getMerId()));
           list.add(map);
        }
        return list;
    }

}
