package com.acg.merchandise.controller;

import com.acg.config.entity.ResponseData;
import com.acg.pojo.Merchandise;
import com.acg.merchandise.service.MerchandiseService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/merchandies")
@Api(tags = "商品列表")

public class MerchandiseController {
    @Autowired
    private MerchandiseService merchandiseService;

    @ApiOperation(value = "无条件查询所有",notes = "查询所有商品的信息")
    @GetMapping("/getMerchandiseAll")
    public ResponseData<Merchandise> getMerchandiseAll(){
        List<Merchandise> merchandiseList =  merchandiseService.findAll();
        if(merchandiseList!= null){
            return  ResponseData.success().putDataVule("merchandiseList",merchandiseList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询一周内增加的商品",notes = "查询所有商品的信息")
    @GetMapping("/listLastWeek")
    public ResponseData<Merchandise> listLastWeek(){
        List<Merchandise> merchandiseList =  merchandiseService.listLastWeek();
        if(merchandiseList!= null){
            return  ResponseData.success().putDataVule("merchandiseList",merchandiseList);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "查询单条记录",notes = "根据id查询商品的信息")
    @GetMapping("/getMerchandiseByid")
    @ApiImplicitParam(value = "资源id",name = "id",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Merchandise> getMerchandiseByid(Integer id){
        Merchandise merchandise =  merchandiseService.findByid(id);
        if(merchandise!= null){
            return  ResponseData.success().putDataVule("merchandise",merchandise);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "上架商品",notes = "新增一条记录 可以输入的参数有sortid, mername,merip,merintroduction,meraddress, merbrand, merpost, merpostway")
    @PostMapping("/insertMerchandise")
    @ApiImplicitParam(value = "查询条件" ,name="merchandise" ,required = true,dataTypeClass = Merchandise.class,paramType = "body")
    public ResponseData<Merchandise> insertMerchandise(@RequestBody Merchandise merchandise){
//        Integer result =  merchandiseService.insertMerchandise(merchandise);
        boolean result =  merchandiseService.save(merchandise);
        if(result){
            return  ResponseData.success().putDataVule("result",result).putDataVule("merchandise", merchandise);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "修改一条记录",notes = "修改一条记录 可以输入的参数有sortid, mername,merip,merintroduction,meraddress, merbrand, merpost, merpostway")
    @PutMapping("/updateMerchandise")
    @ApiImplicitParam(value = "修改内容" ,name="merchandise" ,required = true,dataType = "Merchandise",paramType = "body")
    public ResponseData<Merchandise> updateMerchandise(@RequestBody Merchandise merchandise){
        Integer result =  merchandiseService.updateMerchandise(merchandise);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "删除商品",notes = "根据id删除商品")
    @DeleteMapping("/deleteMerchandise")
    @ApiImplicitParam(value = "商品id" ,name="id" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Merchandise> deleteMerchandise( Integer id){
        Integer result =  merchandiseService.deleteMerchandise(id);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "下架商品",notes = "根据id下架商品")
    @PutMapping("/putoffMerchandise")
    @ApiImplicitParam(value = "删除商品" ,name="id" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Merchandise> putoffMerchandise( Integer id){
        Integer result =  merchandiseService.putoffMerchandise(id);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @PostMapping("/SelectByCondition")
    @ApiOperation(value = "条件查询",notes = "根据条件查询 queryMap中可以包含的参数有sortid mername merstarttime merip meraddress merbrand mmerpost 以及升降序orderByAsc/orderByDesc")
    @ApiImplicitParam(value = "查询条件" ,name="queryMap" ,required = false,dataType = "Map",paramType = "body")
    public ResponseData<Merchandise> SelectByCondition(@RequestBody Map<String,Object> queryMap){
        List<Merchandise> merchandiseList = merchandiseService.QueryByCondition(queryMap);
        if(merchandiseList!= null){
            return  ResponseData.success().putDataVule("merchandiseList",merchandiseList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/SelectByPage")
    @ApiOperation(value = "分页查询",notes = "输入当前页数 每页显示条数查询所有信息")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页数" ,name="pagenum" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示条数" ,name="pagesize" ,required = false,dataType = "Integer",paramType = "query")
    })
    public  ResponseData<Merchandise> SelectByPage(@RequestParam Integer pagenum, @RequestParam Integer pagesize){
        IPage<Merchandise> merListPage =merchandiseService.MerListPage( pagenum, pagesize);
        return ResponseData.success().putDataVule("totla",merListPage.getTotal())
                .putDataVule("pages",merListPage.getPages())
                .putDataVule("merListPage",merListPage.getRecords());
    }
    @PostMapping("/SelectPageByCondition")
    @ApiOperation(value = "分页条件查询",notes ="参数有pagenum pagesize根据条件查询 queryMap中可以包含的参数有")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页数" ,name="pagenum" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "每页显示条数" ,name="pagesize" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "查询条件" ,name="queryMap" ,required = false,dataType = "Map",paramType = "body")
    })
    public ResponseData<Merchandise> SelectPageByCondition(@RequestParam Integer pagenum, @RequestParam Integer pagesize, @RequestBody Map<String, Object> queryMap){
        IPage<Merchandise> merchandiseIPage = merchandiseService.QueryConditionPage(queryMap, pagenum, pagesize);
        return ResponseData.success().putDataVule("totla",merchandiseIPage.getTotal())
                .putDataVule("pages",merchandiseIPage.getPages())
                .putDataVule("merListPage",merchandiseIPage.getRecords());
    }
}