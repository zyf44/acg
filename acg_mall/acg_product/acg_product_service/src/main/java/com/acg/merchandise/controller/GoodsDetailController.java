package com.acg.merchandise.controller;


import com.acg.merchandise.service.CategoryService;
import com.acg.merchandise.service.ImageService;
import com.acg.merchandise.service.MerchandiseService;
import com.acg.merchandise.service.SpecsService;
import com.acg.pojo.Image;
import com.acg.pojo.ImageDetails;
import com.acg.pojo.Merchandise;
import com.acg.pojo.NewImages;
import com.acg.pojo.NewSpecs;
import com.acg.pojo.Specs;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/goods")
public class GoodsDetailController {
    @Autowired
    private MerchandiseService merchandiseService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SpecsService specsService;

    //手办分区页面
    @RequestMapping("/shouban")
    public String shoubanList(Model model){
        return "shouban-list";
    }

    //周边分区页面
    @RequestMapping("/zhoubian")
    public String zhoubianList(Model model){
        return "zhoubian-list";
    }

    //图书分区页面
    @RequestMapping("/tushu")
    public String tushuList(Model model){
        return "tushu-list";
    }

    //游戏分区页面
    @RequestMapping("/youxi")
    public String youxiList(Model model){
        return "youxi-list";
    }

    @RequestMapping("/getMerchandis/{id}") //商品的id
    public String getMerchandisByid(@PathVariable Integer id, Model model){
        //商品信息
        Merchandise merchandise =merchandiseService.findByid(id);
        if (merchandise!=null){
            model.addAttribute("merchandise",merchandise);
        }
        System.out.println(merchandise);
        //图片信息
        Image image =imageService.findByMerid(id);
        if (image!=null){
            //将string转化为list属性
            List<ImageDetails> imagemore = JSONArray.parseArray(image.getImgMore(),ImageDetails.class);
            List<ImageDetails> imageshow = JSONArray.parseArray(image.getImgShow(),ImageDetails.class);
            //将Image转换为NewImage
            NewImages newImage =new NewImages(image.getImgId(),image.getMerId(),imageshow,imagemore);
            model.addAttribute("image",newImage);
            System.out.println(newImage);
        }
        int parentid =categoryService.findByid(merchandiseService.getById(id).getSortId()).getParentId();
        List<Map<String,Object>> list = getPoplar(parentid);
        model.addAttribute("popularMerchandies",list);
        System.out.println(list);
        //获取最低价格
        double lowestprice =specsService.LowestPriceByMerid(id);
        model.addAttribute("lowestprice",(int)lowestprice);
        //规格信息
       List<Specs> specsList =specsService.findbyMerid(id);
       if(specsList!=null){
           List<NewSpecs>  newSpecsList =new ArrayList<>();
           for (int i=0;i<specsList.size();i++){
               Map<String,Object> specsType =JSONObject.parseObject(specsList.get(i).getSpecsType());
               System.out.println(specsType);
               NewSpecs newSpecs =new NewSpecs(specsList.get(i).getSpecsId(),specsList.get(i).getMerId(),specsType,specsList.get(i).getSpecsStock(),specsList.get(i).getSpecsPrice());
               newSpecsList.add(newSpecs);
           }
           model.addAttribute("SpecsList",newSpecsList);
       }else{
           System.out.println("查询规格错误");
       }
        return "goods-detail";
    }
    private   List<Map<String,Object>> getPoplar(int parentid ){
        Map<String,Object> map=null;
        Map<String,Object> map1 =new HashMap<>();
        map1.put("orderByDesc","mer_buy");
        List<Merchandise> merchandises =merchandiseService.QueryByCondition(map1);
        int size=merchandises.size();
        List<Merchandise> newmerchandises =new ArrayList<>();
        for(int i=0;i<size;i++){
            if(categoryService.findByid(merchandises.get(i).getSortId()).getParentId() == parentid){
                System.out.println(i+"是所需的");
                newmerchandises.add(merchandises.get(i));
            }else {
                System.out.println(categoryService.findByid(merchandises.get(i).getSortId()).getParentId() +"是不需要的");
            }
        }
        List<Map<String,Object>> list =new ArrayList<>();
        for(int j =0;j<newmerchandises.size();j++){
            map=new HashMap<>();
            map.put("merid",newmerchandises.get(j).getMerId());
            map.put("name",newmerchandises.get(j).getMerName());
            map.put("image",newmerchandises.get(j).getMerImage());
            map.put("price",(int)specsService.LowestPriceByMerid(newmerchandises.get(j).getMerId()));
            list.add(map);
        }
        return list;
    }

}
