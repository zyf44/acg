package com.acg.merchandise.service;


import com.acg.pojo.NewSpecs;
import com.acg.pojo.Specs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface SpecsService extends IService<Specs> {
    List<Specs> findAll();
    List<Specs>  findbyMerid(Integer merid);
    Integer insertSpecs(NewSpecs newSpecs);
    Integer deleteSpecs(Integer id);
    Integer updateSpece(NewSpecs newSpecs);
    //根据merid查询所有规格最低的价格
    double LowestPriceByMerid(Integer merid);
    Map<String, Object> oneBySpecsId(Integer specsId);
}
