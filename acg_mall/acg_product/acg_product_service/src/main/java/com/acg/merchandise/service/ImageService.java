package com.acg.merchandise.service;

import com.acg.pojo.Image;
import com.acg.pojo.NewImages;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ImageService extends IService<Image> {
  List<Image> findAll();
  Image findByMerid(Integer merid);
  Image findByImgid(Integer imgid);
  Integer insertImage(Image image);
  Integer updateImage(Image image);
  Integer deleteImageByImgid(Integer imgid);
  Integer deleteImageByMerid(Integer merid);
}
