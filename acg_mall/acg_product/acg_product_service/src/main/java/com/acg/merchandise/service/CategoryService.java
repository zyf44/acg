package com.acg.merchandise.service;

import com.acg.pojo.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface CategoryService extends IService<Category> {
    List<Category> findAll();
    List<Category> findByparentid(Integer parentid);
    Category findByid(Integer id);
    Integer insertCategory(Category category);
    Integer deleteCategory(Integer id);
    Integer updateCategory(Category category);
}
