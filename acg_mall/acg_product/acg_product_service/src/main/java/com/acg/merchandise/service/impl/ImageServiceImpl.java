package com.acg.merchandise.service.impl;

import com.acg.merchandise.mapper.ImageMapper;
import com.acg.merchandise.service.ImageService;
import com.acg.pojo.Image;
import com.acg.pojo.NewImages;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements ImageService {
    @Autowired
    private ImageMapper imageMapper;
    @Override
    public List<Image> findAll() {
        return imageMapper.selectList(null);
    }

    @Override
    public Image findByMerid(Integer merid) {
        return imageMapper.selectOne(new QueryWrapper<Image>().eq("mer_id",merid));
    }

    @Override
    public Image findByImgid(Integer imgid) {
        return imageMapper.selectById(imgid);
    }

    @Override
    public Integer insertImage(Image image) {

        return imageMapper.insert(image);
    }

    @Override
    public Integer updateImage(Image image) {

        return imageMapper.updateById(image);
    }

    @Override
    public Integer deleteImageByImgid(Integer imgid) {
        return imageMapper.deleteById(imgid);
    }

    @Override
    public Integer deleteImageByMerid(Integer merid) {
        return imageMapper.delete(new QueryWrapper<Image>().eq("mer_id",merid));
    }

}
