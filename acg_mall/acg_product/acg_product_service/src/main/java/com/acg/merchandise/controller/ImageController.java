package com.acg.merchandise.controller;

import com.acg.config.entity.ResponseData;
import com.acg.merchandise.service.ImageService;
import com.acg.pojo.Image;
import com.acg.pojo.ImageDetails;
import com.acg.pojo.NewImages;
import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/image")
@Api(tags = "图片管理")
public class ImageController {
    @Autowired
    private ImageService imageService;
    @ApiOperation(value = "无条件查询所有",notes = "查询所有图片的信息")
    @GetMapping("/getImageAll")
    public ResponseData<Image> getImageAll(HttpServletRequest request){
        List<Image> imageList =  imageService.findAll();
        if(imageList!= null){
            List<NewImages> newImagesList =new ArrayList<>();
            for (int i=0;i<imageList.size();i++){
                //将string转化为list属性
                List<ImageDetails> imagemore = JSONArray.parseArray(imageList.get(i).getImgMore(),ImageDetails.class);
                List<ImageDetails> imageshow = JSONArray.parseArray(imageList.get(i).getImgShow(),ImageDetails.class);
                //将Image转换为NewImage
                NewImages newImages =new NewImages(imageList.get(i).getImgId(),imageList.get(i).getMerId(),imageshow,imagemore);
                newImagesList.add(newImages);
            }
           request.setAttribute("imageList",newImagesList);
           request.setAttribute("test","测试连通");
            return  ResponseData.success().putDataVule("imageList",newImagesList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询图片根据商品id",notes = "根据merid查询图片信息")
    @GetMapping("/selectListByMerid")
    @ApiImplicitParam(value = "商品id" ,name="merid" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Image> selectListByMerid(Integer merid){
        Image  image=  imageService.findByMerid(merid);
        if(image!= null){
            //将string转化为list属性
            List<ImageDetails> imagemore = JSONArray.parseArray(image.getImgMore(),ImageDetails.class);
            List<ImageDetails> imageshow = JSONArray.parseArray(image.getImgShow(),ImageDetails.class);
            //将Image转换为NewImage
            NewImages newImage =new NewImages(image.getImgId(),image.getMerId(),imageshow,imagemore);
            return  ResponseData.success().putDataVule("image",newImage);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "查询图片根据图片id",notes = "根据imgid查询图片信息")
    @GetMapping("/selectListByImgid")
    @ApiImplicitParam(value = "图片id" ,name="imgid" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Image> selectListByImgid(Integer imgid){
        Image  image=  imageService.findByImgid(imgid);
        if(image!= null){
            //将string转化为list属性
            List<ImageDetails> imagemore = JSONArray.parseArray(image.getImgMore(),ImageDetails.class);
            List<ImageDetails> imageshow = JSONArray.parseArray(image.getImgShow(),ImageDetails.class);
            //将Image转换为NewImage
            NewImages newImage =new NewImages(image.getImgId(),image.getMerId(),imageshow,imagemore);
            return  ResponseData.success().putDataVule("image",newImage);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "新增图片",notes = "新增一个图片")
    @PostMapping("/insertImage")
    @ApiImplicitParam(value = "新增规格" ,name="image" ,required = true,dataTypeClass = Image.class,paramType = "body")
    public ResponseData<Image> insertImage(@RequestBody Image image){
        Integer result =  imageService.insertImage(image);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "修改图片",notes = "修改一个图片")
    @PutMapping("/updateImage")
    @ApiImplicitParam(value = "传入NesImage对象" ,name="image" ,required = true,dataTypeClass = Image.class,paramType = "body")
    public ResponseData<Image> updateImage(@RequestBody Image image){
        Integer result =  imageService.updateImage(image);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "删除图片根据图片id",notes = "根据图片id删除一个图片")
    @DeleteMapping("/deleteByimgid")
    @ApiImplicitParam(value = "图片的id" ,name="imgid" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Image> deleteByimgid(Integer imgid){
        Integer result =  imageService.deleteImageByImgid(imgid);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @ApiOperation(value = "删除图片根据商品id",notes = "根据商品id删除一个图片")
    @DeleteMapping("/deleteByMerid")
    @ApiImplicitParam(value = "商品的id" ,name="merid" ,required = true,dataType = "Integer",paramType = "query")
    public ResponseData<Image> deleteByMerid(Integer merid){
        Integer result =  imageService.deleteImageByMerid(merid);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }

}
