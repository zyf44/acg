package com.acg.merchandise.controller;

import com.acg.merchandise.service.ImageService;
import com.acg.pojo.Image;
import com.acg.pojo.ImageDetails;
import com.acg.pojo.NewImages;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/test")
public class testController {
    @Autowired
    private ImageService imageService;
    @RequestMapping("/test1")
    public String test(){
        return "test";
    }
    @RequestMapping("/test2")
    public String test2(){
        return "redirect:http://wwww.baidu.com";
    }
    @RequestMapping("/getImageList")
    public String getImageList(Model model){
        List<Image> imageList =  imageService.findAll();
        if(imageList!= null){
            List<NewImages> newImagesList =new ArrayList<>();
            for (int i=0;i<imageList.size();i++){
                //将string转化为list属性
                List<ImageDetails> imagemore = JSONArray.parseArray(imageList.get(i).getImgMore(),ImageDetails.class);
                List<ImageDetails> imageshow = JSONArray.parseArray(imageList.get(i).getImgShow(),ImageDetails.class);
                //将Image转换为NewImage
                NewImages newImages =new NewImages(imageList.get(i).getImgId(),imageList.get(i).getMerId(),imageshow,imagemore);
                newImagesList.add(newImages);
            }
            System.out.println(newImagesList);
            model.addAttribute("imageList",newImagesList);
            model.addAttribute("test","测试连通");
        }
        return "test";
    }
}
