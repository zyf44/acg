package com.acg.merchandise.service.impl;

import com.acg.merchandise.mapper.CategoryMapper;
import com.acg.merchandise.service.CategoryService;
import com.acg.pojo.Category;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Category> findAll() {
        return categoryMapper.selectList(null);
    }

    @Override
    public List<Category> findByparentid(Integer parentid) {
        return categoryMapper.selectList(new QueryWrapper<Category>().eq("parent_id",parentid));
    }

    @Override
    public Category findByid(Integer id) {
        return categoryMapper.selectById(id);
    }


    @Override
    public Integer insertCategory(Category category) {
        return categoryMapper.insert(category);
    }

    @Override
    public Integer deleteCategory(Integer id) {
        return categoryMapper.deleteById(id);
    }

    @Override
    public Integer updateCategory(Category category) {
        return categoryMapper.updateById(category);
    }
}
