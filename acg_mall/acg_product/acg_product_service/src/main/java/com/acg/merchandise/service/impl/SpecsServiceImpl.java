package com.acg.merchandise.service.impl;

import com.acg.merchandise.mapper.SpecsMapper;
import com.acg.merchandise.service.SpecsService;
import com.acg.pojo.NewSpecs;
import com.acg.pojo.Specs;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class SpecsServiceImpl extends ServiceImpl<SpecsMapper, Specs> implements SpecsService {
    @Autowired
    private SpecsMapper specsMapper;
    @Override
    public List<Specs> findAll() {
        return specsMapper.selectList(null);
    }

    @Override
    public List<Specs>  findbyMerid(Integer merid) {
        return specsMapper.selectList(new QueryWrapper<Specs>().eq("mer_id",merid));
    }

    @Override
    public Integer insertSpecs(NewSpecs newSpecs) {
        //将specstype从bean对象转化为json格式存储到数据库
        String specstype = JSONObject.toJSONString(newSpecs.getSpecsType());
        System.out.println(specstype);
        Specs specs =new Specs(newSpecs.getSpecsId(),newSpecs.getMerId(),specstype,newSpecs.getSpecsStock(),newSpecs.getSpecsPrice());
        return specsMapper.insert(specs);
    }

    @Override
    public Integer deleteSpecs(Integer id) {
        return specsMapper.deleteById(id);
    }

    @Override
    public Integer updateSpece(NewSpecs newSpecs) {
        //将specstype从bean对象转化为json格式存储到数据库
        String specstype = JSONObject.toJSONString(newSpecs.getSpecsType());
        System.out.println(specstype);
        Specs specs =new Specs(newSpecs.getSpecsId(),newSpecs.getMerId(),specstype,newSpecs.getSpecsStock(),newSpecs.getSpecsPrice());
        return specsMapper.updateById(specs);
    }

    @Override
    public double LowestPriceByMerid(Integer merid) {
     double   lowestprice =  specsMapper.selectList(new QueryWrapper<Specs>().eq("mer_id",merid).orderByAsc("specs_price")).get(0).getSpecsPrice();
        return lowestprice;
    }

//<<<<<<< HEAD
    @Override
    public Map<String, Object> oneBySpecsId(Integer specsId) {
        return specsMapper.oneBySpecsId(specsId);
    }


//=======
//>>>>>>> 123b81b7c50dd299280c7fef9eebb4b2a2bbf81b
}
