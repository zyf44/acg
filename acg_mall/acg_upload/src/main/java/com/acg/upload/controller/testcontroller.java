package com.acg.upload.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/test")
public class testcontroller {
    @RequestMapping("/testpage")
    public String test(){
        return "test";
    }
}
