package com.acg.upload.controller;

import com.acg.upload.config.entity.ResponseData;
import com.acg.upload.details.ImageDetails;
import com.aliyun.oss.OSSClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.alibaba.fastjson.JSON;

//<<<<<<< HEAD

import javax.servlet.http.HttpServletRequest;
//=======
//>>>>>>> 123b81b7c50dd299280c7fef9eebb4b2a2bbf81b
import java.io.File;
import java.io.IOException;
import java.util.*;


@RestController
@RequestMapping("/upload")
@Api(tags = "上传说明")
public class UploadController {
    private static final List<String> CONTENT_TYPES=
            Arrays.asList("image/jpeg","image/gif","image/png");
    //图片在服务器上读取路径
    private static final String BASEURL ="http://localhost:10010/api/merchandise-service/img/";
    //图片在服务器上存放的物理地址
//    private static final String BASEPATH ="E:\\mayun\\acg\\acg_mall\\acg_product\\acg_product_service\\src\\main\\resources\\static\\img\\";
    private static final String BASEPATH ="F:\\BiliBili_Study\\acg\\acg_mall\\acg_product\\acg_product_service\\src\\main\\resources\\static\\img\\";
    @ApiOperation(value = "上传展示图片",notes = "返回的是image_show的list属性")
    @RequestMapping(value="/imageshow/{merid}",method= RequestMethod.POST)
    @ResponseBody
    public ResponseData<String> UploadImageShow(MultipartFile[] files, @PathVariable Integer merid) throws IllegalStateException, IOException {
        if(files.length == 0) {
            return null;
        }
        String path=BASEPATH+merid+"\\";
        File file = new File(path);
        if (file.exists()){
            System.out.println("目录存在");
        }else{
            file.mkdir();
        }
        List<ImageDetails> imageList =new ArrayList<>();
        for (int i = 0; i <files.length; i++) {
            // 获取文件名
            String fileName = files[i].getOriginalFilename();
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //检验文件类型
            String contentType = files[i].getContentType();
            //文件类型不合法，返回null
            if(!CONTENT_TYPES.contains(contentType)){
                System.err.println(fileName+"文件不合法");
                return ResponseData.serverInternalError();
            }
            System.out.println("第" + i + "个文件的源文件名为" + files[i].getOriginalFilename());
            String newname = "show_"+i+suffixName;
            System.out.println("第" + i + "个文件的新文件名为"+newname);
            ImageDetails imageDetails =new ImageDetails();
            imageDetails.setImage(BASEURL+merid+"/"+newname);
            imageList.add(imageDetails);
            file = new File(path + newname);
            //  文件上传
            files[i].transferTo(file);
        }
        String imageshow = JSON.toJSON(imageList).toString();
       // String imageshow2 = imageshow.replace("\\\\","");
        System.out.println(imageshow);
        return  ResponseData.success().putDataVule("imageshow",imageshow);
    }
    @ApiOperation(value = "上传更多图片",notes = "返回的是image_more的字符串")
    @RequestMapping(value="/imagemore/{merid}",method= RequestMethod.POST)
    @ResponseBody
    public  ResponseData<String> UploadImageMore(MultipartFile[] files, @PathVariable Integer merid) throws IllegalStateException, IOException {
        if(files.length == 0) {
            return ResponseData.serverInternalError();
        }
        String path=BASEPATH+merid+"\\";
        File file = new File(path);
        if (file.exists()){
            System.out.println("目录存在");
        }else{
            file.mkdir();
        }
        List<ImageDetails> imageList =new ArrayList<>();
        for (int i = 0; i <files.length; i++) {
            // 获取文件名
            String fileName = files[i].getOriginalFilename();
            // 获取文件的后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //检验文件类型
            String contentType = files[i].getContentType();
            //文件类型不合法，返回null
            if(!CONTENT_TYPES.contains(contentType)){
                System.err.println(fileName+"文件不合法");
                return ResponseData.serverInternalError();
            }
            System.out.println("第" + i + "个文件的源文件名为" + files[i].getOriginalFilename());
            String newname = "more_"+i+suffixName;
            System.out.println("第" + i + "个文件的新文件名为"+newname);
            ImageDetails imageDetails =new ImageDetails();
            imageDetails.setImage(BASEURL+merid+"/"+newname);
            imageList.add(imageDetails);
            file = new File(path + newname);
            //  文件上传
            files[i].transferTo(file);
        }
        String imagemore = JSON.toJSONString(imageList);
        return  ResponseData.success().putDataVule("imagemore",imagemore);
    }
    @RequestMapping(value = "/merimage/{merid}", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传封面图片",notes = "返回的是mer_image的字符串")
    public ResponseData<String> UploadMerImage(MultipartFile file, @PathVariable Integer merid) throws IllegalStateException, IOException {
        if (file.getSize() == 0) {
            return ResponseData.serverInternalError();
        }
        String path=BASEPATH+merid+"\\";
        File newfile = new File(path);
        if (newfile.exists()){
            System.out.println("目录存在");
        }else{
            newfile.mkdir();
        }
        // 获取文件名
        String fileName = file.getOriginalFilename();
        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //检验文件类型
        String contentType = file.getContentType();
        //文件类型不合法，返回null
        if(!CONTENT_TYPES.contains(contentType)){
            System.err.println(fileName+"文件不合法");
            return ResponseData.serverInternalError();
        }
        System.out.println("文件的源文件名为" + file.getOriginalFilename());
        String newname = "cover"+suffixName;
        System.out.println( "文件的新文件名为"+newname);
        newfile = new File(path + newname);
        //  文件上传
        file.transferTo(newfile);
        System.out.println(newfile.getAbsolutePath());
        String merimage =BASEURL+merid+"/"+newname;
        System.out.println(merimage);
        return  ResponseData.success().putDataVule("merimage",merimage).putDataVule("imgUrl", newfile.getAbsolutePath());
    }
    @RequestMapping(value = "/upload1", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "上传图片",notes = "返回图片地址")
    public ResponseData<String> uploadImg(MultipartFile file, HttpServletRequest request) throws IllegalStateException, IOException {
        String realPath = request.getServletContext().getRealPath("/");
        System.out.println(request.getServletPath());
        System.out.println(request.getPathInfo());
        System.out.println(request.getContextPath());
        System.out.println(request.getRequestURI());
        System.out.println(request.getRequestURL());
        System.out.println(realPath);
        if (file.getSize() == 0) {
            return ResponseData.serverInternalError();
        }
        File newfile = new File(realPath);
        if (newfile.exists()){
            System.out.println("目录存在");
        }else{
            newfile.mkdir();
        }
        // 获取文件名
        String fileName = file.getOriginalFilename();
        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //检验文件类型
        String contentType = file.getContentType();
        //文件类型不合法，返回null
        if(!CONTENT_TYPES.contains(contentType)){
            System.err.println(fileName+"文件不合法");
            return ResponseData.serverInternalError();
        }
        System.out.println("文件的源文件名为" + file.getOriginalFilename());
        String newname = System.currentTimeMillis() + suffixName;
        System.out.println( "文件的新文件名为"+newname);
        newfile = new File(realPath + newname);
        //  文件上传
        file.transferTo(newfile);

//        String merimage =BASEURL+merid+"/"+newname;
        String merimage =BASEURL + newname;
        return  ResponseData.success().putDataVule("merimage",merimage);
//        return  ResponseData.success().putDataVule("imgUrl", newfile.getAbsolutePath());
    }
    @Autowired
    OSSClient ossClient;
    @PostMapping("/upload")
    @ResponseBody
    @ApiOperation(value = "上传图片",notes = "返回图片地址")
    public ResponseData<String> upload(HttpServletRequest request, MultipartFile file) throws IOException {
        System.out.println("realPath:\t\t\t" + request.getServletContext().getRealPath("/"));
        String parentPath = request.getServletContext().getRealPath("upload");
        File parent = new File(parentPath);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        String filename = file.getOriginalFilename();
        File dest = new File(parent, filename);
        System.out.println("filename:" + filename);
        String suffixName = filename.substring(filename.lastIndexOf("."));
        String newFilename = System.currentTimeMillis() + suffixName;
        String merImage = "https://qingchengdianshangfy.oss-cn-beijing.aliyuncs.com/" + newFilename;
        file.transferTo(dest);
        ossClient.putObject("qingchengdianshangfy", newFilename, dest);
        // 获取文件的后缀名
        dest.delete();
        return  ResponseData.success().putDataVule("imgUrl", merImage);
    }
    @PostMapping("/uploads")
    @ResponseBody
    @ApiOperation(value = "上传图片",notes = "返回图片地址")
    public ResponseData<List<String>> uploads(HttpServletRequest request, MultipartFile[] files) throws IOException {
        System.out.println("realPath:\t\t\t" + request.getServletContext().getRealPath("/"));
        String parentPath = request.getServletContext().getRealPath("upload");
        File parent = new File(parentPath);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        List<String> imgUrls = new ArrayList<>();
        for (MultipartFile file : files) {
            String filename = file.getOriginalFilename();
            File dest = new File(parent, filename);
            System.out.println("filename:" + filename);
            String suffixName = filename.substring(filename.lastIndexOf("."));
            String newFilename = System.currentTimeMillis() + suffixName;
            String merImage = "https://qingchengdianshangfy.oss-cn-beijing.aliyuncs.com/" + newFilename;
            file.transferTo(dest);
            ossClient.putObject("qingchengdianshangfy", newFilename, dest);
            imgUrls.add(merImage);
            // 获取文件的后缀名
            dest.delete();
        }
        return  ResponseData.success().putDataVule("imgUrls", imgUrls);
    }
}
