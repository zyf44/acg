package com.acg.upload.controller;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.Random;

@Component
public class OSSUtils {

    //todo 这些变量信息自行到阿里云oss存储控制台获取
    //endpoint            替换成自己的
    private String endpoint = "oss-cn-beijing.aliyuncs.com";
    // accessKey        替换成自己的
    //private String accessKeyId = "*************************";
    private String accessKeyId = "LTAI4Fq2BNrPgC2bd1rPC8pB";
    //accessKeySecret     替换成自己的
    //private String accessKeySecret = "*************************";
    private String accessKeySecret = "JSeL5tfNzHrGyBuzmTAIGG6HhIKCmv";
    // Bucket名称        替换成自己的
    private String bucketName = "qingchengdianshangfy";
    // 文件存储目录        替换成自己的
    private String filedir = "*************************";
    private OSSClient ossClient;

    public OSSUtils() {
        //注意： 专有云、专有域环境必须要使用ClientConfiguration的对象传入参数，不然会报错。
        // 创建ClientConfiguration实例，按照您的需要修改默认参数。
        ClientConfiguration conf = new ClientConfiguration();
        // 关闭CNAME选项。
        conf.setSupportCname(false);
        ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret, conf);
    }

    /**
     *          * 初始化
     *         
     */
    public void init() {
        ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
    }

    /**
     *    * 销毁
     *   
     */
    public void destory() {
        ossClient.shutdown();
    }

    /**
     *     * 上传图片
     *    *
     *    * @param url
     *   
     */
    public void uploadImg2Oss(String url) throws Exception {
        File fileOnServer = new File(url);
        FileInputStream fin;
        try {
            fin = new FileInputStream(fileOnServer);
            String[] split = url.split("/");
            this.uploadFile2OSS(fin, split[split.length - 1]);
        } catch (FileNotFoundException e) {
            throw new Exception("图片上传失败1");
        }
    }

    public String uploadImg2Oss(MultipartFile file) throws Exception {
        if (file.getSize() > 10 * 1024 * 1024) {
            throw new Exception("上传图片大小不能超过10M！");
        }
        String originalFilename = file.getOriginalFilename();
        System.out.println(originalFilename);
        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
        Random random = new Random();
        String name = random.nextInt(10000) + System.currentTimeMillis() + substring;
        try {
            InputStream inputStream = file.getInputStream();
            //System.out.println("文件名称="+name);
            this.uploadFile2OSS(inputStream, name);
            return name;
        } catch (Exception e) {
            throw new Exception("图片上传失败2");
        }
    }

    /**
     * 获得图片路径
     *
     * @param fileUrl
     * @return
     */
    public String getImgUrl(String fileUrl) {
        System.out.println("fileUrl=" + fileUrl);
        if (!StringUtils.isEmpty(fileUrl)) {
            String[] split = fileUrl.split("/");
            return this.getUrl(this.filedir + split[split.length - 1]);
        }
        return null;
    }

    /**
     * Description: 判断OSS服务文件上传时文件的contentType 
     *
     * @param filenameExtension 文件后缀
     * @return String
     */
    public static String getcontentType(String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase("gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase("jpeg") || filenameExtension.equalsIgnoreCase("jpg")
                || filenameExtension.equalsIgnoreCase("png")) {
            return "image/jpeg";
        }
        if (filenameExtension.equalsIgnoreCase("html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase("txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase("pptx") || filenameExtension.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase("docx") || filenameExtension.equalsIgnoreCase("doc")) {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase("xml")) {
            return "text/xml";
        }
        return "image/jpeg";
    }

    /**
     * 上传到OSS服务器 如果同名文件会覆盖服务器上的
     *
     * @param instream            文件流
     * @param fileName            文件名称 包括后缀名
     * @return 出错返回"" ,唯一MD5数字签名
     */
    public String uploadFile2OSS(InputStream instream, String fileName) {
        String ret = "";
        try {
            // 创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(instream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getcontentType(fileName.substring(fileName.lastIndexOf("."))));
            objectMetadata.setContentDisposition("inline;filename=" + fileName);
            //System.out.println("开始上传文件");
            // 上传文件
            PutObjectResult putResult = ossClient.putObject(bucketName, filedir + fileName, instream, objectMetadata);
            //PutObjectResult putResult = ossClient.putObject(bucketName,  fileName, instream, objectMetadata);
            //System.out.println("上传成功");
            ret = putResult.getETag();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (instream != null) {
                    instream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     *         		* 获得url链接 
     *          *
     *          * @param key
     *          * @return
     *         
     */
    public String getUrl(String key) {
// 设置URL过期时间为10年 3600l* 1000*24*365*10  

        Date expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
// 生成URL  
        URL url = ossClient.generatePresignedUrl(bucketName, key, expiration);

        if (url != null) {
            System.out.println("url=" + url.toString());
            return url.toString();
        }
        return null;
    }

}
