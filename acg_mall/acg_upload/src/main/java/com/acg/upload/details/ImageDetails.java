package com.acg.upload.details;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImageDetails implements Serializable {
    private String image;
}
