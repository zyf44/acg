package com.acg.user.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("acg_address")
public class Address implements Serializable {
    @TableId(value = "add_id",type = IdType.AUTO)
    private Integer addId;
    @TableField("user_id")
    private Integer userId;
    @TableField("add_province")
    private String addProvince;
    @TableField("add_city")
    private String addCity;
    @TableField("add_country")
    private String addCountry;
    @TableField("detailed_address")
    private String detailedAddress;
    @TableField("add_consignee")
    private String addConsignee;
    @TableField("add_phone")
    private String addPhone;

    public Address() {
    }

    public Address(Integer addId, Integer userId, String addProvince, String addCity, String addCountry, String detailedAddress, String addConsignee, String addPhone) {
        this.addId = addId;
        this.userId = userId;
        this.addProvince = addProvince;
        this.addCity = addCity;
        this.addCountry = addCountry;
        this.detailedAddress = detailedAddress;
        this.addConsignee = addConsignee;
        this.addPhone = addPhone;
    }
}
