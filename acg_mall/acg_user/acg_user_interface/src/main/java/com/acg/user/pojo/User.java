package com.acg.user.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@TableName("acg_user")
@Data
public class User implements Serializable {
    @TableId(value = "user_id",type = IdType.AUTO)
    private Integer userId;
    @TableField("user_phone")
    private String userPhone;
    @TableField("user_password")
    private String userPassword;
    @TableField("user_head")
    private  String userHead;
    @TableField("nick_name")
    private String nickName;
    @TableField("user_sex")
    private String userSex;
    @TableField("user_card_id")
    private  String userCardId;
    @TableField("user_name")
    private  String userName;
    @TableField("creation_time")
    private Date creationTime;
    @TableField("user_status")
    private Integer userStatus;

    public User() {
    }

    public User(Integer userId, String userPhone, String userPassword, String userHead, String nickName, String userSex, String userCardId, String userName, Date creationTime, Integer userStatus) {
        this.userId = userId;
        this.userPhone = userPhone;
        this.userPassword = userPassword;
        this.userHead = userHead;
        this.nickName = nickName;
        this.userSex = userSex;
        this.userCardId = userCardId;
        this.userName = userName;
        this.creationTime = creationTime;
        this.userStatus = userStatus;
    }

    public User(String userPhone, String userHead, String nickName, String userSex, String userCardId, String userName, Integer userStatus) {
        this.userPhone = userPhone;
        this.userHead = userHead;
        this.nickName = nickName;
        this.userSex = userSex;
        this.userCardId = userCardId;
        this.userName = userName;
        this.userStatus = userStatus;
    }
}
