package com.acg.user.service;

import com.acg.user.pojo.Address;
import com.acg.user.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface AddressService extends IService<Address> {
    List<Address> findAll();
    Address findByAddId(Integer addId);
    List<Address> findByUid(Integer uid);
    Integer insertAddress(Address address);
    Integer updateAddress(Address address);
    Integer deleteAddressByUid(Integer uid);
    Integer deleteAddressByAid(Integer addId);
}
