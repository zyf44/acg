package com.acg.user.service.impl;

import com.acg.user.mapper.AddressMapper;
import com.acg.user.pojo.Address;
import com.acg.user.service.AddressService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {
    @Autowired
    private  AddressMapper addressMapper;
    @Override
    public List<Address> findAll() {
        return addressMapper.selectList(null);
    }

    @Override
    public Address findByAddId(Integer addId) {
        return addressMapper.selectById(addId);
    }

    @Override
    public List<Address> findByUid(Integer uid) {
        return addressMapper.selectList(new QueryWrapper<Address>().eq("user_id",uid));
    }

    @Override
    public Integer insertAddress(Address address) {
        return addressMapper.insert(address);
    }

    @Override
    public Integer updateAddress(Address address) {
        return addressMapper.updateById(address);
    }

    @Override
    public Integer deleteAddressByUid(Integer uid) {
        return addressMapper.delete(new QueryWrapper<Address>().eq("user_id",uid));
    }

    @Override
    public Integer deleteAddressByAid(Integer addId) {
        return addressMapper.deleteById(addId);
    }

}
