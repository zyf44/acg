package com.acg.user.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        //设置静态文件的目录
        registry.addResourceHandler("/img/**").addResourceLocations("file:/"+"E:/mayun/acg/acg_mall/acg_product/acg_product_service/src/main/resources/static/img/");
    }
}
