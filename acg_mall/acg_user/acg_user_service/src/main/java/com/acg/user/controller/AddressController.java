package com.acg.user.controller;

import com.acg.user.config.entity.ResponseData;
import com.acg.user.pojo.Address;
import com.acg.user.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@RequestMapping("/address")
@Api(tags = "地址管理")
public class AddressController {
    @Autowired
    private AddressService addressService;
    @GetMapping("/getAll")
    @ApiOperation(value = "无条件查询所有",notes = "查询所有用户的信息")
    public ResponseData<Address> getAll(){
        List<Address> addressList =addressService.findAll();
        if(addressList!= null){
            return  ResponseData.success().putDataVule("addressList",addressList);
        }else{
            return ResponseData.serverInternalError();
        }

    }

    @GetMapping("/findByUId")
    @ApiOperation(value = "查询地址信息根据用户id",notes = "根据用户id查询该用户的所有地址信息")
    @ApiImplicitParam(value = "用户id",name = "uid",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Address> findByUId(Integer uid){
        List<Address> addressList =addressService.findByUid(uid);
        if(addressList!= null){
            return  ResponseData.success().putDataVule("addressList",addressList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/findByAddId")
    @ApiOperation(value = "查询地址信息根据地址id",notes = "根据地址id查询地址信息")
    @ApiImplicitParam(value = "地址id",name = "addid",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Address> findByAddId(Integer addid){
        Address address =addressService.getById(addid);
        if(address!= null){
            return  ResponseData.success().putDataVule("address",address);
        }else{
            return ResponseData.serverInternalError();
        }

    }
    @PostMapping("/InsertAddress")
    @ApiOperation(value = "添加地址",notes = "添加新的地址")
    @ApiImplicitParam(value = "添加新地址信息",name = "address",required = true, dataTypeClass = Address.class,paramType = "body")
    public ResponseData<Address> InsertAddress(@RequestBody Address address){
        Integer result =addressService.insertAddress(address);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @PutMapping("/UpdateAddress")
    @ApiOperation(value = "更新地址",notes = "更新地址")
    @ApiImplicitParam(value = "更新地址信息",name = "address",required = true, dataTypeClass = Address.class,paramType = "body")
    public ResponseData<Address> UpdateAddress(@RequestBody Address address){
        Integer result =addressService.updateAddress(address);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/DeleteAddressByAid")
    @ApiOperation(value = "删除地址信息根据地址id",notes = "根据地址id删除地址")
    @ApiImplicitParam(value = "地址id",name = "addId",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Address> DeleteAddressByAid(Integer addId){
        Integer result =addressService.deleteAddressByAid(addId);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @DeleteMapping("/DeleteAddress")
    @ApiOperation(value = "删除地址信息根据用户id",notes = "根据用户id删除地址")
    @ApiImplicitParam(value = "用户id",name = "uid",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<Address> DeleteAddressByUid(Integer uid){
        Integer result =addressService.deleteAddressByUid(uid);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }

    }


}

