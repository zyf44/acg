package com.acg.user.service.impl;

import com.acg.user.mapper.UserMapper;
import com.acg.user.pojo.User;
import com.acg.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }

    @Override
    public User findByUid(Integer uid) {
        return userMapper.selectById(uid);
    }

    @Override
    public Integer insertUser(User user) {
        user.setCreationTime(new Date());
        String password =user.getUserPassword();
        //使用BCryptPasswordEncoder进行加密
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashPass = bcryptPasswordEncoder.encode(password);
        System.out.println(hashPass);
        user.setUserPassword(hashPass);
        return userMapper.insert(user);
    }

    @Override
    public Integer updateUser(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public Integer deleteUser(Integer uid) {
        return userMapper.deleteById(uid);
    }

    @Override
    public String passWordByPhone(String phone) {
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("user_phone",phone));
        if (user !=null){
            return user.getUserPassword();
        }else {
            return  null;
        }
    }

    @Override
    public User GetUserByPhone(String phone) {
        User user= userMapper.selectOne(new QueryWrapper<User>().eq(
                "user_phone",phone));
        if(user!=null){
            return  user;
        }else{
            return null;
        }
    }

    @Override
    public Boolean CheckPhone(String phone) {
        Boolean flag =true;
        List<User> userList =userMapper.selectList(null);
        for(User user : userList){
            if(user.getUserPhone().equals(phone)){
                flag=false;
            }
        }
        return flag;
    }
}
