package com.acg.user.service;

import com.acg.user.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UserService extends IService<User> {
    List<User> findAll();
    User findByUid(Integer uid);
    Integer insertUser(User user);
    Integer updateUser(User user);
    Integer deleteUser(Integer uid);
    //根据电话（登录账号）返回密码
    String passWordByPhone(String Phone);
    User GetUserByPhone(String  phone);
    //检查电话号码是否唯一
    Boolean CheckPhone(String phone);
}
