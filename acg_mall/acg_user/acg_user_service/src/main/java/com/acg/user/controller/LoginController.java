package com.acg.user.controller;

import com.acg.user.pojo.User;
import com.acg.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/index")
public class LoginController {
    @Autowired
    private UserService userService;
    @RequestMapping("/login")
    public String Login(String  phone, String pass, HttpServletRequest request, HttpServletResponse response)  {
        //判断参数是否存在
        if(phone!=null&&pass!= null){
            String password = userService.passWordByPhone(phone);
            //判断账号是否存在
            if (password != null){
                //解密
                BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
                Boolean result =bcryptPasswordEncoder.matches(pass,password);
                if(result==true){
                    User user =userService.GetUserByPhone(phone);
                    //创建Cookie
                    Cookie cookie = new Cookie("userId", user.getUserId().toString());
                    //设置Cookie的最大生命周期,否则浏览器关闭后Cookie即失效
                    cookie.setMaxAge(Integer.MAX_VALUE);
                    //设置cookie的根路径
                    cookie.setPath("/");
                  // cookie.setHttpOnly(false);
                    //将Cookie加到response中
                    response.addCookie(cookie);
                    //将user存到session中
                    request.getSession().setAttribute("user",user);
                    //重定向到首页
                    try {
                        response.sendRedirect("http://121.199.17.118:8081/home/getpopular");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                System.out.println("账号或密码出错");
            }
        }
        return "login";
    }

    @RequestMapping("/register")
    public String Register()  {
        return "register";
    }

    @RequestMapping("/layout")
    public void  Layout(HttpServletRequest request, HttpServletResponse response){
        //移除session
        request.getSession().removeAttribute("user");
        Cookie cookie = new Cookie("userId", null);
        //设置为0即删除该Cookie
        cookie.setMaxAge(0);
        //删除根路径下的
        cookie.setPath("/");
        response.addCookie(cookie);
        try {
            response.sendRedirect("http://121.199.17.118:8083/index/login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
