package com.acg.user.controller;

import com.acg.user.pojo.Address;
import com.acg.user.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/address")
class AddressModelController {
    @Autowired
    private AddressService addressService;
    @RequestMapping("/GetList/{userid}")
    public String GetList(@PathVariable Integer userid, Model model){
        List<Address> addressList =addressService.findByUid(userid);
        if (addressList!=null){
            model.addAttribute("addressList",addressList);
        }
        return "shoppingAdd";
    }
    @RequestMapping("/Addpage")
    public String AddPage(Integer uid,Model model){
        model.addAttribute("uid",uid);
        return "add";
    }
    @RequestMapping(value = "/UpdatePage",method = RequestMethod.GET)
    public String UpdatePage(Integer aid,Integer uid,Model model){
        Address address =addressService.findByAddId(aid);
        if (address != null){
            model.addAttribute("address",address);
            model.addAttribute("uid",uid);
            return "updateAdd";
        }else{
            System.out.println("查询不到该地址");
            return null;
        }

    }
    @RequestMapping(value = "/Insert",method = RequestMethod.POST)
    public String InsertAddress(@RequestBody Address address){
        Integer result =addressService.insertAddress(address);
        if(result==1){
            Integer uid =address.getUserId();
            String userid =uid.toString();
            return "redirect:/address/GetList/"+userid;
        }else{
            System.out.println("新增地址错误");
            return null;
        }
    }
    @RequestMapping(value = "/Update",method = RequestMethod.POST)
    public Integer UpdateAddress(@RequestBody Address address) {
        Integer result =addressService.updateAddress(address);
        if (result != null){
            return address.getUserId();
        }
        return null;
    }


}
