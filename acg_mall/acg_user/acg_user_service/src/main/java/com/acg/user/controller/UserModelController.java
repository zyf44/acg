package com.acg.user.controller;

import com.acg.user.config.entity.ResponseData;
import com.acg.user.pojo.User;
import com.acg.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/usercent")
public class UserModelController {
    @Autowired
    private UserService userService;
    @RequestMapping(value = "/mycenter/{uid}",method = RequestMethod.GET)
    public String GetUser(@PathVariable Integer uid, Model model){
        User user = userService.findByUid(uid);
        if(user != null){
            model.addAttribute("user",user);
        }else {
            System.out.println("该用户不存在");
        }
        return "myCenter";
    }
    @RequestMapping(value = "/personalInfo/{uid}",method = RequestMethod.GET)
    public String personalInfo(@PathVariable Integer uid, Model model){
        User user = userService.findByUid(uid);
        if(user != null){
            model.addAttribute("user",user);
        }else {
            System.out.println("该用户不存在");
        }
        return "personalInfo";
    }
    @RequestMapping(value = "/editpass/{uid}",method = RequestMethod.GET)
    public String editpass(@PathVariable Integer uid, Model model){
        User user = userService.findByUid(uid);
        if(user != null){
            model.addAttribute("user",user);
        }else {
            System.out.println("该用户不存在");
        }
        return "editpass";
    }
/*    @RequestMapping(value ="/getSession",method = RequestMethod.GET)
    public void getSession(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException
    {
        if(session.isNew())
        {
            response.getWriter().write("new");
            System.out.println("出错");
        }
        else
        {
            String user=(String) session.getAttribute("user");
            System.out.println(user);
            response.getWriter().write(user);
        }
    }*/


}
