package com.acg.user.controller;

import com.acg.user.config.entity.ResponseData;
import com.acg.user.pojo.User;
import com.acg.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "用户管理")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/getAll")
    @ApiOperation(value = "无条件查询所有",notes = "查询所有用户的信息")
    public ResponseData<User> getAll(){
        List<User> userList =userService.findAll();
        if(userList!= null){
            return  ResponseData.success().putDataVule("userList",userList);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/findByUId")
    @ApiOperation(value = "查询单个用户信息",notes = "根据id查询单个用户信息")
    @ApiImplicitParam(value = "用户id",name = "uid",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<User> findByUId(Integer uid){
        User user =userService.findByUid(uid);
        if(user!= null){
            return  ResponseData.success().putDataVule("user",user);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @PostMapping("/InsertUser")
    @ApiOperation(value = "添加用户",notes = "添加新的用户")
    @ApiImplicitParam(value = "新增用户的信息",name = "user",required = true, dataTypeClass = User.class,paramType = "body")
    public ResponseData<User> InsertUser(@RequestBody User user) {
        //保证电话号码的唯一
        Integer result = userService.insertUser(user);
        if (userService.CheckPhone(user.getUserPhone())) {
                return ResponseData.success().putDataVule("result", result);
        } else {
            return ResponseData.serverInternalError();
        }
    }
    @PostMapping("/UpdatetUser")
    @ApiOperation(value = "修改用户",notes = "修改用户的信息")
    @ApiImplicitParam(value = "修改用户的信息",name = "user",required = true, dataTypeClass = User.class,paramType = "body")
    public ResponseData<User> UpdatetUser(@RequestBody User user){
        Integer result =userService.updateUser(user);
        //保证电话号码的唯一
        if (userService.CheckPhone(user.getUserPhone())) {
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @DeleteMapping("/findByUId")
    @ApiOperation(value = "删除用户",notes = "根据id删除用户")
    @ApiImplicitParam(value = "用户id",name = "uid",required = true, dataType = "Integer",paramType = "query")
    public ResponseData<User> DeleteUser(Integer uid){
       Integer result = userService.deleteUser(uid);
        if(result!= null){
            return  ResponseData.success().putDataVule("result",result);
        }else{
            return ResponseData.serverInternalError();
        }
    }
    @PostMapping("/Login")
    @ApiOperation(value = "验证登录",notes = "验证登录")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "登录账号（电话号码）" ,name="phone" ,required = true,dataType = "String",paramType = "query"),
            @ApiImplicitParam(value = "登录密码" ,name="pass" ,required = true,dataType = "String",paramType = "query")
    })
    public ResponseData<User> Login(String  phone, String pass){
        String password = userService.passWordByPhone(phone);
        //解密
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        Boolean result =bcryptPasswordEncoder.matches(pass,password);
        if(result==true){
            System.out.println("登录成功！");
            return  ResponseData.success().putDataVule("result",result);
        }else{
            System.out.println("账号或密码出错");
            return ResponseData.serverInternalError();
        }
    }
    @GetMapping("/UpdatePassword")
    @ApiOperation(value = "更改密码",notes = "更改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "用户id" ,name="uid" ,required = true,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "新的密码" ,name="pass" ,required = true,dataType = "String",paramType = "query")
    })
    public ResponseData<User> UpdatePassword(Integer uid,String pass){
         User user =userService.findByUid(uid);
        if (user !=null){
            //加密
            BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
            String newpass =bcryptPasswordEncoder.encode(pass);
            user.setUserPassword(newpass);
            Integer result= userService.updateUser(user);
            if(result!=null){
                return  ResponseData.success().putDataVule("result",result);
            }
        }
        return ResponseData.serverInternalError();
    }
}
