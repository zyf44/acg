function sendRegister() {
    if($("input[name='pass1']").val() == $("input[name='pass2']").val() && $("input[name='pass1']").val().length >= 6) {
        var queryMap = {"userPassword": $("input[name='pass1']").val(),
                        "userPhone": $("input[name='phone']").val()};
        $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            contentType: "application/json",
            //请求地址
            url : "/user/InsertUser",
            //数据，json字符串
            data : JSON.stringify(queryMap),
            //请求成功
            success : function(result) {
                console.log(result);
                window.location.href = "/index/login";
            },
            //请求失败，包含具体的错误信息
            error : function(e) {
                console.log(e.status);
                console.log(e.responseText);
                alert("注册失败");
            }
        });
    }
}