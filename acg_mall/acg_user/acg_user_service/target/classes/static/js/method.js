/*根据id获取对象*/
function $(str) {
    return document.getElementById(str);
}

var addrShow = $('addr-show');
var btn = document.getElementsByClassName('btn')[0];
var prov = $('prov');
var city = $('city');
var country = $('country');
var aname =$('edit-name');
var tel  = $('edit-Tel');
var add = $('edit-addr');
// var detail=$('detail-add')


/*用于保存当前所选的省市区*/
var current = {
    prov: '',
    city: '',
    country: ''
};

/*自动加载省份列表*/
(function showProv() {
    btn.disabled = true;
    var len = provice.length;
    for (var i = 0; i < len; i++) {
        var provOpt = document.createElement('option');
        provOpt.innerText = provice[i]['name'];
        provOpt.value = i;
        prov.appendChild(provOpt);
    }
})();

/*根据所选的省份来显示城市列表*/
function showCity(obj) {
    var val = obj.options[obj.selectedIndex].value;
    // console.log(prov);
    console.log(val);
    if (current.prov != val) {
        console.log(current.prov)
        // console.log(current.prov.options[o]);
        current.prov = val;
        console.log(current.prov);
        // console.log(prov.innerText);
        addrShow.value = '';
        btn.disabled = true;
    }
    //console.log(val);
    if (val != null) {
        console.log(city);
        city.length = 1;
        var cityLen = provice[val]["city"].length;
        for (var j = 0; j < cityLen; j++) {
            var cityOpt = document.createElement('option');
            cityOpt.innerText = provice[val]["city"][j].name;
            cityOpt.value = j;
            city.appendChild(cityOpt);
        }
    }
}

/*根据所选的城市来显示县区列表*/
function showCountry(obj) {
    var val = obj.options[obj.selectedIndex].value;
    current.city = val;
    if (val != null) {
        country.length = 1; //清空之前的内容只留第一个默认选项
        var countryLen = provice[current.prov]["city"][val].districtAndCounty.length;
        if (countryLen === 0) {
            addrShow.value = provice[current.prov].name + '-' + provice[current.prov]["city"][current.city].name;
            return;
        }
        for (var n = 0; n < countryLen; n++) {
            var countryOpt = document.createElement('option');
            countryOpt.innerText = provice[current.prov]["city"][val].districtAndCounty[n];
            countryOpt.value = n;
            country.appendChild(countryOpt);
        }
    }
}

/*选择县区之后的处理函数*/
function selecCountry(obj) {
    current.country = obj.options[obj.selectedIndex].value;
    if ((current.city != null) && (current.country != null)) {
        btn.disabled = false;
    }
    this.showAddr();

}

/*点击确定按钮显示用户所选的地址*/
function showAddr() {
    addrShow.value = provice[current.prov].name + '-' + provice[current.prov]["city"][current.city].name + '-' + provice[current.prov]["city"][current.city].districtAndCounty[current.country];
}
//提交整个表单
function sub(addid,uid) {
    console.log(addrShow.value + aname.value + tel.value + add.value)
    console.log(provice[current.prov].name)
    console.log(addid + "   " + uid)
    var addcity = provice[current.prov]["city"][current.city].name;
    console.log("城市" + addcity);
    var addcountry = provice[current.prov]["city"][current.city].districtAndCounty[current.country];
    console.log("县区" + addcountry);
    var addpro = provice[current.prov].name;
    console.log("省份" + addpro)
    var addname = aname.value;
    console.log("姓名" + addname)
    var detailadd = add.value
    console.log("详细地址" + detailadd)
    var telnum = tel.value;
    if ((addrShow.value != "") && (aname.value != "") && (tel.value != "") && (add.value != "")) {
        var adata = {
            "addId": addid,
            "userId": uid,
            "addProvince": addpro,
            "addCity": addcity,
            "addCountry": addcountry,
            "detailedAddress": detailadd,
            "addConsignee": addname,
            "addPhone": telnum
        };
        var data = JSON.stringify(adata);
        jQuery.ajax({
            url: "http://localhost:8083/address/Update",
            data: data,
            dataType: "json",
            contentType : "application/json",  //返回的数据是json 格式
            type: "POST",
            success: function (data) {
                // const message = new Message();
                //    message.show({
                //        type: 'success',
                //        text: '提交成功'
                //    });
                alert("修改成功并返回管理页面？")
                //返回上一页面
                window.history.back(-1);
                // jQuery.ajax({
                //     url:"/address/GetList/"+uid,
                //     // data:{},
                //     // dataType: "json",
                //     // contentType : "application/json",  //返回的数据是json 格式
                //     // success: function (data) {
                //     //
                //     // },
                //     error:function () {
                //
                //     }
                // });

            }
        });

    }

}
function subAdd() {
    userid = document.getElementById("peiqipeiqi")
    uid = userid.value
    console.log(userid)
    console.log(uid)
    var addcity = provice[current.prov]["city"][current.city].name;
    console.log("城市" + addcity);
    var addcountry = provice[current.prov]["city"][current.city].districtAndCounty[current.country];
    console.log("县区" + addcountry);
    var addpro = provice[current.prov].name;
    console.log("省份" + addpro)
    var addname = aname.value;
    console.log("姓名" + addname)
    var detailadd = add.value
    console.log("详细地址" + detailadd)
    var telnum = tel.value;
    var adata = {
        "userId": uid,
        "addProvince": addpro,
        "addCity": addcity,
        "addCountry": addcountry,
        "detailedAddress": detailadd,
        "addConsignee": addname,
        "addPhone": telnum
    };
    var data = JSON.stringify(adata);
    jQuery.ajax({
        url: "/address/InsertAddress",
        data: data,
        dataType: "json",
        contentType : "application/json",  //返回的数据是json 格式
        type: "POST",
        success: function (res) {
            if(res.status === 200){
                alert("添加成功")
                window.history.back(-1);
            }

        },
        error:function(){
            alert("添加失败")

        }

    });

}



