package com.acg.cart.controller;

import com.acg.cart.service.AlipayService;
import com.acg.cart.service.CartService;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private AlipayService alipayService;
    @Autowired
    private CartService cartService;

    @RequestMapping("/pay")
    public void payMent(double price,Integer userid, HttpServletResponse response, HttpServletRequest request) {
        try {
            alipayService.aliPay(price,response, request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //清空购物车
        cartService.deleteAllcart(userid);
    }
    @RequestMapping("/success")
    public String ReturnSuccess() {
       return "success";
    }
}