package com.acg.cart.config;

public  class AlipayConfig {
	// 商户appid
	public static String APPID = "2016102900775890";
	// 私钥 pkcs8格式的
	public static String RSA_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCSv0MzL0lRgR2L8o65X+/htpOWoAIw6aHSDXvy+WmslB475uazVtwOKSsvf2N/4kGDyqT8QXFoSN58v9aeG/ekkuBq1FLrrn6u0SiCx59GJfaJHdK1L8aSh+xJhGmtmcL/ETNfrh+NjF6WoQVndS3NCDFyDkH8u72CE22+KnwSK+WtOyImS5dRsiID01MzdaK8gpSAlzAAAKvn3a/+9iSguEWShUgUFq3wmRfDCZ+c0oRGf04xcN6IwogcTy55JKXRVeXGA8Q8oRIxwFW2vsHK5DMxlIZvF5f1lJSBDQablCcS6+OG4Cx7CgC1SzzVfSkk3dCxFoEvoH4RLYPpTFNhAgMBAAECggEAaPXKP+LzgwthscZ01RxMfFnVaGfcCVYh+xbP7/L65Ygvs9KscduPm8VQcTGhXkfbakNYDGFWlyt/jyS7ge0pDJpNT7u773yoeat6WUgkoMzocBxXqGaalhYIKHFAEP4onb2UzDvDr7+VrBFRxzTl5H+WtiiChB1YJ6Bz3w+OHIQaBk5O7iST0HKNSD3epDhe4N4u8CladdlYGoDNrnvCw7sCh0kuDtC2aD1sWmK7wtdXOIP5vbT5/W3ThghmuslQY1/PdyUsQsmHOV6A1OG3TgtPi9PJaWHW7N+mBOHLbjt338F5cipKyQBhacKKXJAGZLvYrZWQn39Qkez+Nt7CbQKBgQDDJH5iRwwIr3Y0KOuz2J3m3m62pCrHcIW7fy2xJGbkWjQkrFct/6wi4ILdIZTd1jM2zzbTGG6HIFBI8b13i9JhTljUljcuPoTNLXlXUH63dD2+oZ/6fo/HFNKi356SZ8Vg2DwUDVzkxxKpimBOFJdGJSfCO4MFrRvOz0nJCFRV9wKBgQDAgwt2YF4oQWRIzcZf4JXSC28DC5z+mTw4rfnWUtk2akGedYOt2NnF4bRkNt3ka1+u5ZymRteaSm0kAgIe+xVsBUoW1X98badi1g3YYu06qsi+aklgUELIcn15yMELQHTrHoxOIshmJJKPbJAs74khtQxHG3qqP2Hpc7ieEizrZwKBgEYw2TjWX1C8dCL82RMehmVleqM9V5nXMnMnorLF6jCW9Yx/ZPhJr4v3/3o0uww0IMXFMO4rKo52JAqo0UKvy2Nuv+rvgTekCzaV9cBfVsT8Db2E+zspzOOJ7N3i23HD264i+TEpQVGU/XR4Ln3HUEZN1Qg7Sbx/8LGpw9uIoHUvAoGBAIBsKw5Z9RX3oXWryAUXpfpL5E9sxNtWz8Sj0oG2HO7e7U1O6XvkqEoIajVcIKQLOPZMeDa4FYX41lvp9w5BwyWfVbBr1jLkV4BoeS37uHOlkGruLmrQv+xF7Ax87sKZyiU66LALJhJ1nUuOSZOga6OPylP5Mdg2lhONPgA88bhLAoGAVksaNKqHcLjYFpJerV8E7aPRIfnV67cVWyQZ40VqadC/I60IX1a9O2xsaj8xVbtDJk+GFFHMJjQ7aDdd5clAZKXhpcpd+zlqh2wqv3jzw3pQc5uHgEBtC87++pQAb8hALGU89ldcAKahYXp8K5xYRVfCGe7Bp5/NUMOsexOzZLY=";
	// 服务器异步通知页面路径
	/*public static String notify_url = "http://localhost:8091/payment/success";*/
	// 页面跳转同步通知页面路径 支付成功跳转回首页
	public static String return_url = "http://121.199.17.118:8081/home/getpopular";
	// 请求网关地址
	public static String URL = "https://openapi.alipaydev.com/gateway.do";
	// 编码
	public static String CHARSET = "UTF-8";
	// 返回格式
	public static String FORMAT = "json";
	// 支付宝公钥
	public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApvnBFJcP3KA55U/ewdMghq4g0SSQYb3LnqCym51oOlauqfuKoa7P+pUXwrBlJZHg5PIY6tsUE/Bfn6i+9rTjOfLRIGQdwVldGJVixF6EJHLfdBgfCF4DIaTurjyIUG1XmcIhkWY5cIHMiGfGuDAKdVgAGddlL6bp6XMAN2oXNut/x6qWy0yChzTjRmjTGsyFZMsjf49CHKspZyaUHv30do9r3VcMpYPMR7z3CvAKB3FNWQ79Tn5Gh8IoOL0CWKU9riWjNJFNQ0WOezR1y7W0VT1IGi0sN72/qi2tNC+RVKYzbF+Lghw2ciOSJcBbscJJUKzW71c7I58Lg5SIlC664QIDAQAB";
	// 日志记录目录
	public static String log_path = "/log";
	// RSA2
	public static String SIGNTYPE = "RSA2";
}
