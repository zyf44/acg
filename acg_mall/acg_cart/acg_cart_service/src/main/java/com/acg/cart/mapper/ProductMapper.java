package com.acg.cart.mapper;

import com.acg.pojo.Merchandise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductMapper extends BaseMapper<Merchandise> {
}
