package com.acg.cart.controller;

import com.acg.cart.service.CartService;
import com.acg.config.entity.ResponseData;
import com.acg.pojo.Cart;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
@Api(tags = "购物车操作")
public class CartController {
    @Autowired
    private CartService cartService;

    @ApiOperation(value = "无条件查询所有",notes = "查询所有购物车的信息")
    @GetMapping("/getCartAll")
    public ResponseData<Cart> getCartAll(){
        List<Cart> cartList =  cartService.findAll();
        if(cartList!= null){
            return  ResponseData.success().putDataVule("cartList",cartList);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @ApiOperation(value = "根据用户id查询用户的购物车",notes = "查询用户所有购物车")
    @GetMapping("/getCartByid")
    @ApiImplicitParam(value = "用户id" ,name="id" ,required = false,dataType = "Integer",paramType = "query")
    public ResponseData<Cart> getCartByID(@RequestParam(required = false) Integer id){
        List<Cart> cartList =  cartService.findCartById(id);
        if(cartList!= null){
            return  ResponseData.success().putDataVule("cartList",cartList);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/getCheckedCart/{id}")
    public List<Cart> getCheckedCart(@PathVariable Integer id){
        List<Cart> list =  cartService.findCheckerCart(id);
        return  list;
    }



    @ApiOperation(value = "根据用户id分页查询用户的购物车",notes = "分页查询用户所有购物车")
    @GetMapping("/CartPageByid")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "用户id" ,name="id" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "当前页数" ,name="num" ,required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "每页条数" ,name="size" ,required = false,dataType = "Integer",paramType = "query")
    })
    public ResponseData<Cart> CartPageByID(@RequestParam Integer id,@RequestParam Integer num,@RequestParam Integer size){
        IPage<Cart> cartPage =  cartService.cartPageByid(id,num,size);
        return ResponseData.success().putDataVule("totla", cartPage.getTotal())
                .putDataVule("pages", cartPage.getPages())
                .putDataVule("brandList", cartPage.getRecords());
    }

    @PostMapping("/addCart")
    @ApiOperation(value="增加购物车",notes="增加相应的购物车信息")
    @ApiImplicitParam(value = "购物车" ,name="cart" ,required = false,dataType = "Cart",paramType = "body")
    public ResponseData<Cart> addCart(@RequestBody Cart cart){
        Integer res = cartService.addCart(cart);
        if(res == 1){
            return ResponseData.success();
        }else{
            if(res == 0){
                return  ResponseData.cusstomerError("库存不足,请重新选择购买数量!");
            }
            return ResponseData.serverInternalError();
        }
    }

    @PostMapping("/updateCart")
    @ApiOperation(value="更新购物车数量",notes="更新购物车的产品数量")
    @ApiImplicitParam(value = "购物车" ,name="cart" ,required = false,dataType = "Cart",paramType = "body")
    public ResponseData<Cart> updateCart(@RequestBody Cart cart){
        Integer res = cartService.updateProduct(cart);
        Cart cart1 = cartService.findCart(cart.getCartid());
        if(res == 1){
            return ResponseData.success().putDataVule("cart",cart);
        }else{
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/deleteCart")
    @ApiOperation(value="删除某个购物车",notes="根据id删除某个购物车")
    @ApiImplicitParam(value = "购物车id" ,name="cartid" ,required = false,dataType = "Integer",paramType = "query")
    public ResponseData<Cart> deleteCart(@RequestParam Integer cartid) {
        Integer res = cartService.deleteCart(cartid);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/deleteAllCart")
    @ApiOperation(value="清空用户全部购物车",notes="清空购物车")
    @ApiImplicitParam(value = "用户id" ,name="userid" ,required = false,dataType = "Integer",paramType = "query")
    public ResponseData<Cart> clearCart(@RequestParam Integer userid) {
        Integer res = cartService.deleteAllcart(userid);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }
    }

    @GetMapping("/radioCart")
    @ApiOperation(value = "对某个购物车进行单选或者取消单选",notes ="改变购物车选中状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "购物车id",name = "cartid",required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "选中状态",name = "checked",required = false,dataType = "Integer",paramType = "query")
    })
    public ResponseData<Cart> radioCart(@RequestParam Integer cartid , @RequestParam Integer checked){
        Integer res = cartService.radioCart(cartid,checked);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }

    }

    @GetMapping("/allCheckedCart")
    @ApiOperation(value = "全选或取消全选",notes ="改变全选状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "用户id",name = "userid",required = false,dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(value = "选中状态",name = "checked",required = false,dataType = "Integer",paramType = "query")
    })
    public ResponseData<Cart> allCheckedOrUnchecked(@RequestParam Integer userid , @RequestParam Integer checked){
        Integer res = cartService.allCheckedOrUnchecked(userid,checked);
        if (res == 1) {
            return ResponseData.success();
        } else {
            return ResponseData.serverInternalError();
        }

    }
}
