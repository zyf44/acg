package com.acg.cart.mapper;

import com.acg.pojo.Image;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImagMapper extends BaseMapper<Image> {
}
