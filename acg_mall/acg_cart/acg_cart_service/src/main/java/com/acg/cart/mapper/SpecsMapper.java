package com.acg.cart.mapper;

import com.acg.pojo.Specs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SpecsMapper extends BaseMapper<Specs> {
}
