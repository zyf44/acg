package com.acg.cart.controller;


import com.acg.cart.service.CartService;
import com.acg.config.entity.ResponseData;
import com.acg.pojo.Cart;
import com.acg.pojo.CartVo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@RequestMapping("/cart")
public class CartShowController{
    @Autowired
    private CartService cartService;
    @RequestMapping("/getCartList/{id}")
    public String getCartByID(@PathVariable Integer id, Model model){
        List<CartVo> cartvolist = cartService.cartvoList(id);
        List<Map<String,Object>> list = new ArrayList<>();
        for(CartVo cartVo:cartvolist){
            Map<String,Object> map =new HashMap<>();
            map.put("cartid",cartVo.getCartid());//购物车id
            map.put("userid",cartVo.getUserid());//用户id
            map.put("cartamount",cartVo.getCartamount());//商品数量
            map.put("cartprice",cartVo.getCartprice());//商品价格
            String newimg = null;
            if(cartVo.getImg().indexOf(".") == 0){
                String a = "../";
                newimg = a+cartVo.getImg();
            }else{
                newimg = cartVo.getImg();
            }
            map.put("image",newimg);//商品图片地址
            map.put("checked",cartVo.getCartchecked());//商品勾选状态
            map.put("productname",cartVo.getProductname());//商品名称
            map.put("specid",cartVo.getSpecsid());//商品规格id
            map.put("color",cartVo.getSpecsDetails().getColor());//商品颜色
            map.put("size",cartVo.getSpecsDetails().getSize());//商品尺寸
            map.put("material",cartVo.getSpecsDetails().getMaterial());//商品材料
            map.put("weight",cartVo.getSpecsDetails().getWeight());//商品重量
            list.add(map);
        }
        model.addAttribute("list",list);
        return "myCart";
    }

    @RequestMapping("/deleteCart1/{cartid}")
    public String deletecart(@PathVariable Integer cartid, Model model){
        Cart cart = cartService.findCart(cartid);
        Integer id = cart.getUserid();
        cartService.deleteCart(cartid);
        return "redirect:/cart/getCartList/"+id;
    }
    @RequestMapping("/count")
    public String Count(){
        return "count";
    }
}
