package com.acg.cart.service;

import com.acg.pojo.Cart;
import com.acg.pojo.CartVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

public interface CartService extends IService<Cart> {
    List<Cart> findAll();
    Cart findCart(Integer cartid);
    List<Cart> findCartById(Integer userid);
    List<Cart> findCheckerCart(Integer userid);
    IPage<Cart> cartPageByid(Integer userid,Integer pagenum,Integer pagesize);
    Integer addCart(Cart cart);
    Integer updateProduct(Cart cart);
    Integer deleteCart(Integer cartid);
    Integer deleteAllcart(Integer userid);
    Integer  radioCart(Integer cartid,Integer checked);
    Integer  allCheckedOrUnchecked(Integer userid,Integer checked);
    List<CartVo> cartvoList(Integer userid);
}
