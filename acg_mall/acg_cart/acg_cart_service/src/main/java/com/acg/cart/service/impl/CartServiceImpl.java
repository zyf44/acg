package com.acg.cart.service.impl;

import com.acg.cart.mapper.CartMapper;
import com.acg.cart.mapper.ImagMapper;
import com.acg.cart.mapper.ProductMapper;
import com.acg.cart.mapper.SpecsMapper;
import com.acg.cart.service.CartService;
import com.acg.pojo.*;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;


@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements CartService{
    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private SpecsMapper specsMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ImagMapper imagMapper;
    @Override
    public List<Cart> findAll() {
        return cartMapper.selectList(null);
    }

    @Override
    public Cart findCart(Integer cartid) {
        return cartMapper.selectById(cartid);
    }

    @Override
    public List<Cart> findCartById(Integer userid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userid);
        return cartMapper.selectList(queryWrapper);
    }

    @Override
    public List<Cart> findCheckerCart(Integer userid) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userid);
        queryWrapper.eq("cart_checked",1);
        return cartMapper.selectList(queryWrapper);
    }

    @Override
    public IPage<Cart> cartPageByid(Integer userid, Integer pagenum, Integer pagesize) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userid);
        IPage<Cart> page = new Page<>(pagenum,pagesize);
        page = cartMapper.selectPage(page,queryWrapper);
        return page;
    }

    @Override
    public Integer addCart(Cart cart) {
        Cart cart2 = null;
        int flag = 0;
        Integer spec_id = cart.getSpecsid();
        Specs specs = specsMapper.selectById(spec_id);
        Integer stork = specs.getSpecsstock();
        Integer user_id = cart.getUserid();
        List<Cart> cartList = findCartById(user_id);
        for (Cart cart1 : cartList) {
            if (cart1.getSpecsid().equals(spec_id)){
                cart2 = cart1;
                flag = 1;
            }
        }
        if(flag == 0) {
            try {
                if(stork < cart.getCartamount())
                    return 0;
                cartMapper.insert(cart);
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }else if(flag == 1) {
            cart2.setCartamount(cart.getCartamount() + cart2.getCartamount());
            cart2.setCartprice(cart2.getCartprice().add(cart.getCartprice()));
            try {
                if(stork < cart2.getCartamount())
                    return 0;
                cartMapper.updateById(cart2);
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        }
        return  -1;
    }

    @Override
    public Integer updateProduct(Cart cart) {
        try {
            cartMapper.updateById(cart);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public Integer deleteCart(Integer cartid) {
        cartMapper.deleteById(cartid);
        return  1;
    }

    @Override
    public Integer deleteAllcart(Integer userid) {
        try {
            QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userid);
            cartMapper.delete(queryWrapper);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public Integer radioCart(Integer cartid, Integer checked) {
        try {
            Cart cart = findCart(cartid);
            cart.setCartchecked(checked);
            cartMapper.updateById(cart);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }

    }

    @Override
    public Integer allCheckedOrUnchecked(Integer userid,Integer checked) {
        try {
            QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userid);
            Cart cart = new Cart();
            cart.setCartchecked(checked);
            cartMapper.update(cart, queryWrapper);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<CartVo> cartvoList(Integer userid) {
        List<Cart> cartlist = findCartById(userid);
        List<Specs> specslist = new ArrayList<>();
        List<String> imagelist = new ArrayList<>();
        List<String> productnamelist = new ArrayList<>();
        List<CartVo> cartVolist = new ArrayList<>();
        for(Cart cart:cartlist){
            Integer specid = cart.getSpecsid();
            Specs specs = specsMapper.selectById(specid);
            specslist.add(specs);
            Merchandise merchandise =  productMapper.selectById(specs.getMerid());
            productnamelist.add(merchandise.getMername());
            QueryWrapper<Image> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("mer_id",merchandise.getMerid());
            Image image = imagMapper.selectOne(queryWrapper);
            imagelist.add(image.getImgshow());
        }
        for(int i = 0;i < cartlist.size();i++){
            CartVo cartVo = new CartVo();
            cartVo.setUserid(userid);
            cartVo.setCartid(cartlist.get(i).getCartid());
            cartVo.setCartamount(cartlist.get(i).getCartamount());
            cartVo.setCartprice(cartlist.get(i).getCartprice());
            cartVo.setCartchecked(cartlist.get(i).getCartchecked());
            cartVo.setSpecsid(cartlist.get(i).getSpecsid());
            cartVo.setProductname(productnamelist.get(i));
            List<ImageDetails> imageshow = JSONArray.parseArray(imagelist.get(i),ImageDetails.class);
            cartVo.setImg(imageshow.get(0).getImage());
            SpecsDetails specstype= JSONObject.parseObject(specslist.get(i).getSpecstype(),SpecsDetails.class);
            cartVo.setSpecsDetails(specstype);
            cartVolist.add(cartVo);
        }
        return  cartVolist;
    }
}
