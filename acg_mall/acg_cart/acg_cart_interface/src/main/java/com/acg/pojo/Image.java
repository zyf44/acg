package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@TableName("acg_image")
public class Image implements Serializable {
    @TableId(value = "img_id",type = IdType.AUTO)
    private Integer imgid;
    @TableField("mer_id")
    private Integer merid;
    @TableField("img_show")
    private String imgshow;
    @TableField("img_more")
    private String imgmore;
    public Image() {
    }

    public Image(Integer imgid, Integer merid, String imgshow, String imgmore) {
        this.imgid = imgid;
        this.merid = merid;
        this.imgshow = imgshow;
        this.imgmore = imgmore;
    }

    public Integer getImgid() {
        return imgid;
    }

    public void setImgid(Integer imgid) {
        this.imgid = imgid;
    }

    public Integer getMerid() {
        return merid;
    }

    public void setMerid(Integer merid) {
        this.merid = merid;
    }

    public String getImgshow() {
        return imgshow;
    }

    public void setImgshow(String imgshow) {
        this.imgshow = imgshow;
    }

    public String getImgmore() {
        return imgmore;
    }

    public void setImgmore(String imgmore) {
        this.imgmore = imgmore;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imgid=" + imgid +
                ", merid=" + merid +
                ", imgshow='" + imgshow + '\'' +
                ", imgmore='" + imgmore + '\'' +
                '}';
    }
}
