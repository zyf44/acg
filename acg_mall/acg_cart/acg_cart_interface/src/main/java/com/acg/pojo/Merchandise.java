package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


import java.io.Serializable;
import java.util.Date;
@TableName("acg_merchandise")
@Data
public class Merchandise implements Serializable {
    @TableId(value = "mer_id" ,type = IdType.AUTO)
    private Integer merid;
    @TableField("sort_id")
    private Integer sortid;
    @TableField("mer_name")
    private String mername;
    @TableField("mer_ip")
    private String merip;
    @TableField("mer_start_time")
    private Date merstarttimme;
    @TableField("mer_end_time")
    private Date merendtime;
    @TableField("mer_introduction")
    private  String merintroduction;
    @TableField("mer_status")
    private Integer merstatus;
    @TableField("mer_address")
    private String meraddress;
    @TableField("mer_brand")
    private String merbrand;
    @TableField("mer_post")
    private Integer merpost;
    @TableField("mer_post_way")
    private String merpostway;
    @TableField("mer_collection")
    private Integer mercollection;
    @TableField("mer_image")
    private String merimage;
    @TableField("mer_buy")
    private Integer merbuy;
    @TableField("mer_response")
    private Integer merresponse;

    public Merchandise(Integer merid, Integer sortid, String mername, String merip, Date merstarttimme, Date merendtime, String merintroduction, Integer merstatus, String meraddress, String merbrand, Integer merpost, String merpostway, Integer mercollection, String merimage, Integer merbuy, Integer merresponse) {
        this.merid = merid;
        this.sortid = sortid;
        this.mername = mername;
        this.merip = merip;
        this.merstarttimme = merstarttimme;
        this.merendtime = merendtime;
        this.merintroduction = merintroduction;
        this.merstatus = merstatus;
        this.meraddress = meraddress;
        this.merbrand = merbrand;
        this.merpost = merpost;
        this.merpostway = merpostway;
        this.mercollection = mercollection;
        this.merimage = merimage;
        this.merbuy = merbuy;
        this.merresponse = merresponse;
    }

    public Merchandise() {
    }

    public Merchandise(Integer sortid, String mername, String merip, String merintroduction, String meraddress, String merbrand, Integer merpost, String merpostway, String merimage) {
        this.sortid = sortid;
        this.mername = mername;
        this.merip = merip;
        this.merintroduction = merintroduction;
        this.meraddress = meraddress;
        this.merbrand = merbrand;
        this.merpost = merpost;
        this.merpostway = merpostway;
        this.merimage = merimage;
    }



}

