package com.acg.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartVo {
    private Integer cartid;
    private Integer userid;
    private Integer specsid;
    private Integer cartamount;
    private BigDecimal cartprice;
    private SpecsDetails specsDetails;
    private String productname;
    private String img;
    private  Integer cartchecked;
}
