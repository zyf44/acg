package com.acg.pojo;

import java.io.Serializable;

public class SpecsDetails implements Serializable {
    private String color;
    private String size;
    private String weight;
    private String  material;

    public SpecsDetails() {
    }

    public SpecsDetails(String color, String size, String weight, String material) {
        this.color = color;
        this.size = size;
        this.weight = weight;
        this.material = material;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "SpecsDetails{" +
                "color='" + color + '\'' +
                ", size='" + size + '\'' +
                ", weight='" + weight + '\'' +
                ", material='" + material + '\'' +
                '}';
    }
}